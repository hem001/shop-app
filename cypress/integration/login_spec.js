describe('Login Page', () => {
  beforeEach(()=>{
    cy.visit('/login');
  })
  
it('greets with login',()=>{
  cy.contains('h2','Login')
})

it('requires email',()=>{
  cy.get('form').contains('Log in').click()
  cy.get('.ant-alert').should('contain', 'please enter both fields')
})
it('requires password',()=>{
  cy.get('input[name=email]').type('hem{enter}')
  cy.get('.ant-alert').should('contain', 'please enter both fields')
})
it('requires valid username and password',()=>{
  cy.get('input[name=email]').type('hem')
  cy.get('input[name=pwd]').type('asdf{enter}')
  cy.get('.ant-alert').should('contain', 'You have entered wrong details')
})
it('successfull login',()=>{
  cy.get('input[name=email]').type('hem')
  cy.get('input[name=pwd]').type('password{enter}')
  cy.get('.ant-message').should('contain', 'Successfully Logged in');


})

  
})