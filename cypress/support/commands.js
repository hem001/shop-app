
/*Cypress.Commands.add('login',()=>{
  cy.visit('/login')
  cy.get('input[name=email]').type('hem')
  cy.get('input[name=pwd]').type('password{enter}')
  cy.get('.ant-message').should('contain', 'Successfully Logged in');

})*/

Cypress.Commands.add('login',()=>{
  cy.request({
    method:'POST',
    url:'http://localhost:8000/api/login',
    body:{
        email:'hem',
        password:'password',
        admin_user: false,
      }
  })
  .then((resp)=>{
    window.localStorage.setItem('app-token', resp.body.token)
  })
}
