import React from "react";
import ReactDOM from "react-dom";
import { applyMiddleware, createStore } from "redux";
import logger from 'redux-logger';
import { Provider } from "react-redux";

import {RouterComponent} from "./components/RouterComponent";
//redux-persist
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react'


import {rootReducer} from './redux/reducers';

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer);

//logger should be the last middleware to be added to store
const store = createStore(persistedReducer, applyMiddleware(logger));
const persistor = persistStore(store);

class AppWrapper extends React.Component {
  render() {
    return <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
    	<RouterComponent />
    </PersistGate>

    
    </Provider>;
  }
}

ReactDOM.render(<AppWrapper/>, document.getElementById("root"));
