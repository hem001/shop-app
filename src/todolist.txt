new task:ctrl+enter
complete task: alt+d
fold sections: -- and tab
date: cr and tab tab
created @date: ctrl+shift+enter

--- ✄ -----------------------
TODO List:
  ☐ Testing 
  ☐ Payment by paypal
  
 --- ✄ -----------------------
 ☐  @created(July 16, 2020)
 ✓ order function is almost complete. last thing -need to add paypal payment method. @done (July 16, 2020)
 
 --- ✄ -----------------------
 ☐  @created(July 15, 2020)
 ☐ CMS-OrderPage
   ✓ created AllOrdersList, CompletedOrdersList, PendingOrdersList and OrderTable components @done (July 15, 2020)
   ✓ changed OrderMainPage component so that it can be reused by all other order list displaying components @done (July 15, 2020)
   ✓ Backend: created http:GET to get list of orders @done (July 15, 2020)
 
 --- ✄ -----------------------
 ☐  @created(July 13, 2020)
   ✓ CMS - Inventory page add comments, make them deleteable @done (July 13, 2020)
   ✓ changed inventory api in backend to async await functions @done (July 13, 2020)
   ☐ 
 
  --- ✄ -----------------------
  ☐  @created(June 29, 2020)
    ☐ Orders - 
      ☐ frontend- cms
        ☐ main order page(list orders by unique order id and sort by date created)
        ✓ individual order page @done (July 13, 2020)

      ✓ backend - order CRUD functions @done (July 13, 2020)
        ☐ 
    ☐ CMS inventory item page
      ✓ show all items in a table @done (July 13, 2020)
      ☐ use editable table 
--- ✄ -----------------------

 ☐  @created(July 06, 2020)
   ✓ changed Categories nav bar to show categories submenu @done (July 06, 2020)
    ✓ Category view in customer side- list and grid views @done (July 06, 2020)
    ✓ connected the item list in the category view to the item details @done (July 06, 2020)

--- ✄ -----------------------
 ☐  @created(June 22, 2020)
   ✓ removing from shopping cart: ask before removing. @done (June 22, 2020) using Modal confirm
   ✓ when adding items to cart, check if there are previous copies. if there are add it instead of making a new copy.- makes sure there is only one copy in the cart. @done (June 22, 2020)
   ✓ update cart after changing the quantities of items in shopping cart. @done (June 22, 2020)
   ✓ add orders to the CMS to take action @done (July 13, 2020)

--- ✄ -----------------------
 ☐  @created(20-06-21 16:43)
 ✓ finish removing cart item list from the store. (todo) @done (June 22, 2020)
     still need to take care of redundant items in the cart. it removes all matching items using filter().

 --- ✄ -----------------------
 

 ☐  @created(March 02, 2020)
 ItemPage: Ratings Tab
 ✓ Calculate Ratings and render them @done (March 02, 2020)
 ✓ started shopping-cart page @done (July 13, 2020)

 
--- ✄ -----------------------
 ☐  @created(February 26, 2020)
 ITEM PAGE
 ✓ posting comments to server @done (February 26, 2020)
 ✓ the review saved to server does not render wihtout refreshing. @done (March 02, 2020)
 ✓ Post Comment: min rating set to 1. @done (March 02, 2020)


--- ✄ -----------------------
created(20-02-25)
 ☐ @done
✔ edit item : when submitted without changing anything- wrong value @done (20-02-25 15:17)
✔ edit items - backend: done @done (20-02-25 15:18)
✔ ItemPage: add comment -frontend @done (20-02-25 15:28)
✔ ItemPage: add comment- posted comment to server. @done (20-02-25 16:51)
--- ✄ -----------------------
created(20-02-04)
 ✔ AddRemoveIputField completed and passed the image variables to the parent component @done (20-02-17 12:38)
 ✔ need to work on getting data in InputPair and using it. @done (20-02-25 15:19)
 ✔ Delte Page for items - frontend @done (20-02-25 15:19)
 ✔ delete items - backend @done (20-02-25 15:19)
 ✔ Edit Page for items- frontend @done (20-02-25 15:19)
 ✔ edit items -backend @done (20-02-25 15:19)

--- ✄ -----------------------
created(20-02-17)
 ☐ AddRemoveInputFields
 ✔ add remove button for each button @done (20-02-17 09:20)
 ✔ add one Add button.check when prev input is empty it does not add new input field @done (20-02-17 09:20)



--- ✄ -----------------------

created(20-02-07)
edit item page
 ✔ search bar with button @done (20-02-07 12:13)
 ✔ search by product id @done (20-02-07 12:13)
 ☐ when item found show items in input fields
 ☐ add save & cancel button at the bottom
 
--- ✄ -----------------------
created(20-02-04)
 Delete Page
 ✔ search bar with search button @done (20-02-05 16:11)
 ✔ delete button @done (20-02-05 16:11)
 ✔ iframe below to display product @done (20-02-05 16:11)
 ✔ search by product id: @done (20-02-05 16:11)
 ✔ delete funtion for api to delete from mongo server.@done (20-02-05 16:11)

--- ✄ -----------------------
@ created(20-02-04 10:35)
✔ check-error when other_details is null in addItemPage3. @done (20-02-04 11:20)
 
 