import axios from 'axios';
import {
  HOME_PAGE_DATA_URL,
  GET_ALL_ITEMS_URL,
  GET_ITEM_BY_ID_URL,
  ADD_ITEM_URL,
  DELETE_ITEM_BY_ID_URL,
  DELETE_ONE_COMMENT_URL,
  EDIT_ITEM_BY_ID_URL,
  EDIT_ITEM_BY_KEY_URL,
  ADD_REVIEW_BY_ID_URL,
  GET_CATEGORIES_LIST_URL,
  GET_ITEMS_BY_CATEGORY_URL,

} from '../utils/constants';

export const ItemData = {
  getHomePageData,
  getAllItems,
  getItemById,
  addItemData,
  deleteItemById,
  editItemById,
  editItemByKey,
  addReviewById,
  getCategoriesList,
  getItemsByCategory,
  deleteOneComment,
};

function getHomePageData() {
  var promise = axios.get(HOME_PAGE_DATA_URL);
  return promise;
}

function getAllItems() {
  var promise = axios.get(GET_ALL_ITEMS_URL);
  return promise;
}

function getItemById(id) {
  var promise = axios.get(GET_ITEM_BY_ID_URL + '/' + id);
  return promise;
}

function addItemData(data) {
  var promise = axios.post(ADD_ITEM_URL, data);
  return promise;
}

function deleteItemById(id) {
  var url = DELETE_ITEM_BY_ID_URL + '/' + id;
  var promise = axios.delete(url);
  return promise;
}
function deleteOneComment(data) {
  var promise = axios.put(DELETE_ONE_COMMENT_URL, data);
  return promise;
}

function editItemById(data) {
  var url = EDIT_ITEM_BY_ID_URL + '/' + data._id;
  var promise = axios.put(url, data);
  return promise;

}
function editItemByKey(obj) {
  var url = EDIT_ITEM_BY_KEY_URL ;
  
  var promise = axios.put(url, obj);
  return promise;
}

function addReviewById(data) {
  var url = ADD_REVIEW_BY_ID_URL + '/' + data._id;
  var promise = axios.put(url, data);
  return promise;

}

function getCategoriesList() {
  var promise = axios.get(GET_CATEGORIES_LIST_URL);
  return promise;
}

function getItemsByCategory(category) {
  var promise = axios.get(GET_ITEMS_BY_CATEGORY_URL + "/" + category);
  return promise;
}