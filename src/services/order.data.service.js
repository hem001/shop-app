import axios from 'axios';
import {
  GET_ORDER_LIST_URL,
  ADD_ORDER_URL,
  DELETE_ORDER_BY_ID_URL,
  GET_ORDER_BY_ID_URL,
  GET_PENDING_ORDERS_LIST_URL,
  GET_COMPLETED_ORDERS_LIST_URL,
  COMPLETE_ORDER_URL,
  UNDO_COMPLETE_ORDER_URL,

} from '../utils/constants';

export const OrderData = {
  getOrderList,
  getOrderById,
  addOrder,
  getPendingOrdersList,
  getCompletedOrdersList,
  completeOrder,
  undoCompleteOrder,
  
};
function undoCompleteOrder(order_id) {
  var promise = axios.put(UNDO_COMPLETE_ORDER_URL, {order_id:order_id});
  return promise;
}

function completeOrder(order_id) {
  var promise = axios.put(COMPLETE_ORDER_URL, {order_id:order_id});
  return promise;
}
function getPendingOrdersList() {
  var promise = axios.get(GET_PENDING_ORDERS_LIST_URL);
  return promise;
}
function getCompletedOrdersList() {
  var promise = axios.get(GET_COMPLETED_ORDERS_LIST_URL);
  return promise;
}

function getOrderList() {
  var promise = axios.get(GET_ORDER_LIST_URL);
  return promise;
}

function getOrderById(order_id) {
  var promise = axios.get(GET_ORDER_BY_ID_URL + '/' + order_id);
  return promise;
}

function addOrder(data) {
  var promise = axios.post(ADD_ORDER_URL, data);
  return promise;
}