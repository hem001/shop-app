import axios from 'axios';

import { APIURL } from '../utils/constants';

export const Auth = {
  saveToken,
  getToken,
  register_admin,
  register,
  login,
  isLoggedIn,
  isAdminLoggedIn,
  logout,
};

function saveToken(token) {
  localStorage.setItem('app-token', token);
  // console.log('token saved', token);
}

function getToken() {
  return localStorage.getItem('app-token');
}

function register_admin(user) {
  var promise = axios.post(APIURL + '/register-admin',
    user
  );
  return promise;
}

function register(user) {
  var promise = axios.post(APIURL + '/register',
    user
  );
  return promise;
}

function login(user) {
  // console.log(user);
  var promise = axios.post(APIURL + '/login',
    user
  );
  return promise;
}

function isLoggedIn() {
  var token = this.getToken();
  if (token) {
    var payload = JSON.parse(window.atob(token.split('.')[1]));
    return (payload.exp > Date.now() / 1000);
  } else {
    return false;
  }
}

function isAdminLoggedIn() {
  return (this.isLoggedIn && localStorage.getItem('adminLoggedIn'));
}

function logout() {
  localStorage.removeItem('app-token');
  if (localStorage.getItem('adminLoggedIn')) {
    localStorage.removeItem('adminLoggedIn');
  }
}