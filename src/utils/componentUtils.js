export function onInputChange(event) {
  // let value = event.target.value;
  const { name, value } = event.target;
  this.setState({
    [name]: value
  });
}

export function showModal() {
  this.setState({
    show_modal: true
  });
}
export function closeModal() {
  this.setState({
    show_modal: false
  });
}

export function objectToList(arg) {
  let retVal = [];
  Object.keys(arg).map(i => retVal.push([i, arg[i]]));
  return retVal;
}

export function listToObject(arg) {
  let retVal = {};
  for (var i = 0; i < arg.length; i++) {
    if (arg[i][0].trim()) {
      retVal[arg[i][0]] = arg[i][1];
    }
  }
  return retVal;
}
