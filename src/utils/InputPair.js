import React, { Component } from 'react';

import {Input, Button, message, Tooltip} from 'antd'

//pair needs to be a 2D-list [['a','b'],['c','d']]
//use objectToList() function from componentUtils before passing as props
//then create a function called onDataChange that updates the value of the data.
export class InputPair extends Component {
  constructor(props){
    super(props);
    
    this.handleChange = this.handleChange.bind(this);
    this.handleOnBlur = this.handleOnBlur.bind(this);
    this.handleRemoveButton = this.handleRemoveButton.bind(this);
    this.handleAddButton = this.handleAddButton.bind(this);

  }

  handleAddButton(){
    let d = this.props.data;
    let l = d.length;
    
    if(l){
      console.log('last item', d[l-1][0] === ''|| d[l-1][1]==='');
      if(d[l-1][0] === '' || d[l-1][1] === ''){
        console.log('err last item', d[l-1]);
      message.error('Please enter both fields');
      return;
      }}
    d.push(['','']);
    this.props.onDataChange(d);
  }

  handleChange= i => e => {
    let data = [...this.props.data];
    if (e.target.name === "first") {
      data[i][0] = e.target.value;
    } else {
      data[i][1] = e.target.value;
    }
    this.props.onDataChange(data);
  }
  handleOnBlur(){

  }
  handleRemoveButton=i=>e=>{
    let d = this.props.data;
    d.splice(i,1);
    this.props.onDataChange(d);

  }

  render() {
      return (
        <React.Fragment >
        <div className="input-pair-style">
        {this.props.data.map((i,j)=>
          <div className="" key={j}>
          <Input
            style={{ width: "30%", margin: "5px" }}
            placeholder="Item Detail"
            onChange={this.handleChange(j)}
            onBlur = {this.handleOnBlur}
            value = {i[0]}
            name="first"
            type="text"
          />

          <Input
            style={{ width: "40%", margin: "5px" }}
            placeholder="Description"
            onChange={this.handleChange(j)}
            name="second"
            type="text"
            value = {i[1]}
          />
          <Tooltip title="Remove details">
          <Button
            icon="minus"
            type="primary"
            style={{ margin: "5px" }}
            name={j}
            onClick={this.handleRemoveButton(j)}
          ></Button>
          </Tooltip>
          </div>)}
                </div>
        <Tooltip title="Add more details">
          <Button 
          icon="plus" 
          type="primary" 
          onClick={this.handleAddButton}
          name ="add">
            Add
          </Button>
        </Tooltip>
          
        </React.Fragment>
      );
  }
}
  
