export const APIURL = 'http://localhost:8000/api';
export const URL = 'http://localhost:3000/';

export const HOME_PAGE_DATA_URL = APIURL +'/getAllItems';
export const GET_ALL_ITEMS_URL = APIURL +'/getAllItems';
export const GET_ITEM_BY_ID_URL = APIURL +'/getItemByID';
export const CMSDASHBOARD_URL = "/cms-dashboard";
export const ADD_ITEM_URL = APIURL +'/addItem';
export const DELETE_ITEM_BY_ID_URL = APIURL + '/deleteItem';
export const EDIT_ITEM_BY_ID_URL = APIURL + '/editItem';
export  const ADD_REVIEW_BY_ID_URL = APIURL + '/addReviewById';
export const   EDIT_ITEM_BY_KEY_URL = APIURL +'/editItemByKey';
export const DELETE_ONE_COMMENT_URL = APIURL +'/deleteOneComment';

//orders
export const GET_ORDER_LIST_URL = APIURL +"/getOrderList";
export const GET_ORDER_BY_ID_URL = APIURL + "/getOrderById";
export const ADD_ORDER_URL = APIURL +"/addOrder";
export const DELETE_ORDER_BY_ID_URL = APIURL +"/deleteOrderById";
export const GET_PENDING_ORDERS_LIST_URL = APIURL + "/getPendingOrdersList";
export const GET_COMPLETED_ORDERS_LIST_URL = APIURL + "/getCompletedOrdersList";
export const COMPLETE_ORDER_URL = APIURL + "/orderCompleted";
export const UNDO_COMPLETE_ORDER_URL = APIURL +"/undoOrderCompleted";

//categories

export const GET_CATEGORIES_LIST_URL = APIURL + '/getCategoriesList';
export const GET_ITEMS_BY_CATEGORY_URL = APIURL + '/getItemsByCategory';


