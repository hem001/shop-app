import React, { Component } from "react";
import { Input, Button, Tooltip } from "antd";


class AddRemoveInputFields extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: []
    };
    this.handleAddButton = this.handleAddButton.bind(this);
    this.handleRemoveButton = this.handleRemoveButton.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }
  componentDidMount(){
    this.setState({
      inputs: this.props.inputs
    });
    this.props.imageChange(this.state.inputs);

  }

  handleInputChange(e){
    const {name, value} = e.target;
    const tmp = this.state.inputs;
      tmp[name] = value;
      this.setState({
        inputs: tmp,
      });

      this.props.imageChange(this.state.inputs);
    

  }
  handleAddButton() {
    //donot add new input fields if previously added field is empty
    const l = this.state.inputs.length
    if((l && this.state.inputs[l-1].trim()) || l===0){
      this.setState({
        inputs: [...this.state.inputs, '']
      })
    }
  }


  handleRemoveButton(e) {
    // console.log(e.target);
    const i = e.target.name;
    
    const tmp = this.state.inputs;
    tmp.splice(i,1);
      // console.log(tmp);
    this.setState({
      inputs: tmp,
    })
    this.props.imageChange(tmp);

  }
  render() {
    return (
      <React.Fragment>
        {this.state.inputs.map((i, j) => (
          <div className="" key={j}>
            <Input
            style={{width:'70%'}} 
            value={i} 
            type="text"
            onChange={this.handleInputChange}
            name = {j}
             />

            <Tooltip title="Remove details">
              <Button
                icon="minus"
                type="primary"
                style={{ margin: "5px" }}
                name={j}
                onClick={this.handleRemoveButton}
              ></Button>
            </Tooltip>
          </div>
        ))}
        <Tooltip title="Add more details">
          <Button
          icon= "plus"
            type="primary"
            style={{ margin: "5px" }}
            onClick={this.handleAddButton}
            name='add'
          >Add</Button>
        </Tooltip>
      </React.Fragment>
    );
  }
}


export {
  AddRemoveInputFields,
}