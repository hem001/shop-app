import React, { Component } from "react";
import { Menu, Icon } from "antd";
import { connect } from "react-redux";
import { authActions } from "../../../redux/actions/";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import {Auth} from "../../../services";


const { SubMenu } = Menu;

class CMSNavBar extends Component {
  constructor(props) {
    super(props);
    this.clickedLogout = this.clickedLogout.bind(this);
  }

  clickedLogout() {
    this.props.history.push("/cms");
    Auth.logout();
    this.props.dispatch(authActions.logoutAdmin());
  }
  render() {
    return (
      <div>
        <Menu mode="inline" theme="dark" defaultOpenKeys={["item", "orders"]}>
          <SubMenu key="item" title={<span>Item</span>}>
          <Menu.Item>
            <Link to="/cms-dashboard/inventory" >Inventory</Link>
          </Menu.Item>
            <Menu.Item>
              <Link to="/cms-dashboard/addItem">Add New Item</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/cms-dashboard/editItem">Edit Item</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/cms-dashboard/deleteItem">Delete Item</Link>
            </Menu.Item>
          </SubMenu>
          <SubMenu key="orders"
          title="Orders">
            <Menu.Item>
              <Link to="/cms-dashboard/pending-orders">
              Pending Orders</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/cms-dashboard/completed-orders">Completed Orders</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/cms-dashboard/all-orders">All Orders</Link>
            </Menu.Item>
          </SubMenu>
          
          <Menu.Item onClick={this.clickedLogout}>
            <Icon type="logout" /> Logout
          </Menu.Item>
        </Menu>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    username: state.auth.username,
    isAdminLoggedIn: state.auth.isAdminLoggedIn,
  };
};

const connectedNav = connect(mapStateToProps, null)(CMSNavBar);
const cms = withRouter(connectedNav);
export { cms as CMSNavBar };
