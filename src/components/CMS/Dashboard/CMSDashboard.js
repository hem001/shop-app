import React, { Component } from "react";
import { connect } from "react-redux";
import { Switch, Route, Link } from "react-router-dom";
import { Layout } from "antd";
import { CMSNavBar } from "./CMSNavBar";
import { AddItem } from "../Content/AddItem/AddItem";
import { EditItem } from "../Content/EditItem";
import { AddItemPage2 } from "../Content/AddItem/AddItemPage2";
import { AddItemPage3 } from "../Content/AddItem/AddItemPage3";
import { CMSHomePage } from "../Content/CMSHomePage";
import { CMSDeleteItem } from "../Content/CMSDeleteItem";

import { OrderMainPage } from "../Content/Orders/OrderMainPage";
import { OrderItemPage } from "../Content/Orders/OrderItemPage";
import { AllOrdersList } from "../Content/Orders/AllOrdersList";
import { CompletedOrdersList } from "../Content/Orders/CompletedOrdersList";
import { PendingOrdersList } from "../Content/Orders/PendingOrdersList";

import { InventoryMainPage } from "../../Inventory/InventoryMainPage";
import { CategoryView} from "../../Inventory/CategoryView";
import {InventoryItem} from "../../Inventory/InventoryItem";


import "../../../sass/cms-dashboard.sass";

const { Header, Footer, Sider, Content } = Layout;

class CMSDashboard extends Component {
  render() {
    return (
      <React.Fragment>
        <Layout>
          <Header>
            <div className="header-style">
              <Link to="/">CMS Dashboards</Link>
            </div>
          </Header>
          <Layout>
            <Sider>
              <CMSNavBar />
            </Sider>

            <Content className="content-box">
              <Switch>
                <Route exact path="/cms-dashboard" component={CMSHomePage} />
                <Route path="/cms-dashboard/inventory"
                  component={InventoryMainPage} />
                <Route
                  path="/cms-dashboard/category/:category"
                  component={CategoryView}
                />
                <Route path="/cms-dashboard/item/:item_id" component={InventoryItem} />

                <Route path="/cms-dashboard/addItem">
                  <AddItem />
                </Route>

                <Route
                  path="/cms-dashboard/addItemPage2"
                  component={AddItemPage2}
                />
                <Route
                  path="/cms-dashboard/addItemPage3"
                  component={AddItemPage3}
                />
                <Route
                  path="/cms-dashboard/deleteItem"
                  component={CMSDeleteItem}
                />

                <Route path="/cms-dashboard/editItem" component={EditItem} />
                <Route path="/cms-dashboard/all-orders" component={AllOrdersList} />
                <Route path="/cms-dashboard/completed-orders" component={CompletedOrdersList} />
                <Route path="/cms-dashboard/pending-orders" component={PendingOrdersList} />
                
                <Route
                  path="/cms-dashboard/order/:order_id"
                  component={OrderItemPage}
                />
                <Route 
                path="/*" render={()=>{return <div className="title">Page not Found</div>}}
                />

              </Switch>
            </Content>
          </Layout>

          <Footer>Footer</Footer>
        </Layout>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    username: state.auth.username,
  };
};

const conCMS = connect(mapStateToProps, null)(CMSDashboard);
export { conCMS as CMSDashboard };
