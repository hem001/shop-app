import React, { Component } from "react";
import {withRouter} from 'react-router-dom'
import { Form, Input, Button, message } from "antd";
import {ItemData} from "../../../services";
import {AddRemoveInputFields} from "../../../utils/AddRemoveInputFields";
import {InputPair} from "../../../utils/InputPair";
import {objectToList, listToObject} from '../../../utils/componentUtils';

import "../../../sass/delete.item.sass";


const { Search } = Input;
const nList = ['__v', '_id','category','images', 'other_details','price', 'quantityToAdd','item_ratings','comments' ];
const numType = ['price', 'quantityToAdd'];

class EditItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: {},
      filteredItem: {},
      hasLoaded: false,
      notFound: true,
      images:[],
      data:[] ,
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onImageInputChange = this.onImageInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onDataChange = this.onDataChange.bind(this);
  }
  handleSubmit(){
    console.log('on submit ');
    if(this.state.images.length <1){
      message.error('Please choose at least one image of the product');
      return;
    }
    let tmp = {...this.state.item, images: this.state.images};
    if(this.state.data){
      
     let o_data = listToObject(this.state.data);
     // console.log(o_data, this.state.data);
     tmp = {...tmp, other_details: o_data}
    }
    
    
    

    this.setState({ 
      item : tmp,
    })
    
    //setState is asynchronous so pass tmp instead
    ItemData.editItemById(tmp)
    .then(response =>{
      message.success('Successfully edited item');
        console.log('Success');
      this.props.history.push('/cms-dashboard');
    }
    ,error=>{
      message.error('Error editing the item');
      console.log('Error ');
    });


  }
  onDataChange(data){
    this.setState({
      data,
    });
  }
  onImageInputChange(data){
    this.setState({
      images: data,
    });
  }
  onInputChange(event) {
    // let value = event.target.value;
    const { name, value } = event.target;
    

    this.setState(prevState => ({
      item: { ...prevState.item, [name]: value }
    }));
  }
  handleSearch(value) {
    ItemData.getItemById(value).then(
      response => {
        this.setState({
          item: response.data.item,
          hasLoaded: true,
          notFound: false
        });
        // console.log(this.state.item);
        //filter item and separate others and images keys
        if(this.state.item.other_details){
        var data = objectToList(this.state.item.other_details);
        }
        this.setState({
          data: data,
          images: this.state.item.images,
        })
      },
      error => {
        console.log("Error ", error.message);
        this.setState({
          hasLoaded: true,
          notFound: true,
          error_message: "Error: Item not Found."
        });
        message.error("Item not found");
      }
    );
  }
  render() {
    return (
      <div className="delete-item-container">
        <h2 align="center">Edit Item</h2>

        <Search
          placeholder="Enter product id"
          enterButton="Search"
          size="large"
          onSearch={value => this.handleSearch(value)}
          name="search"
        />

        <div className="page-display-box">
          {this.state.hasLoaded ? (
            this.state.notFound ? (
              <p>Item not found </p>
            ) : (
            <div>
              <EditDiv
                item={this.state.item}
                handleChange={this.onInputChange}
                imageChange = {this.onImageInputChange}
                o_data = {this.state.data}
                onDataChange={this.onDataChange}
              />
              <Button type="primary"
            className="delete-form-button"
            onClick={()=>{this.setState({item:{}}); this.props.history.push('/cms-dashboard')}} >Cancel</Button>
              <Button type="primary"
            className="delete-form-button"
            onClick = {this.handleSubmit}
            >Submit</Button>
              </div>
            )
          ) : (
            <p> Waiting to Load</p>
          )}
        </div>
      </div>
    );
  }
}
export const EditDiv = ({ item, handleChange, imageChange, o_data, onDataChange}) => {
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 }
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 }
    },
    labelAlign: "left"
  };
  return (
    <React.Fragment>
    <Form {...formItemLayout}>
      {Object.keys(item).map((i,j)=>
        (!nList.includes(i))?(
          <Form.Item key={j} label={i}>
           <Input
           name ={i}
           value ={item[i]}
           onChange ={handleChange}
           type = 'text'
           /> 
          </Form.Item>
          ):((numType.includes(i)?(
            <Form.Item key={j} label={i}>
             <Input
             name ={i}
             value ={item[i]}
             onChange ={handleChange}
             type ='number'
             min ={0}
             /> 
            </Form.Item>
            ) :(null)))
        
        )}
      <Form.Item label="Category">
       <Input
       name ="category"
       value ={item.category}
       onChange ={handleChange}
       type ='number'
       /> 
      </Form.Item>
      <Form.Item label="Images">


      <AddRemoveInputFields inputs ={item.images}  imageChange={imageChange} />
       
      </Form.Item>
      {o_data ? (<div className="">
        <p>Other Detail:</p>
        <InputPair data={o_data} onDataChange = {onDataChange} /></div>
        ):null}
      
      </Form>
    

    </React.Fragment>

  )
}




const c = withRouter(EditItem);
export { c as EditItem};