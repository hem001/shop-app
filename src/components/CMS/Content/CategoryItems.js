import React, { Component } from 'react';
import {Row,Col,Divider} from 'antd';

import {ItemData} from '../../../services';
import '../../../sass/home.sass';


class CategoryItems extends Component {
  constructor(props){
    super(props);
    this.state={
      data_loaded: false,
      data:[],
    };
  }
  componentDidMount(){
    console.log('hello params ',this.props.match.params.category);
    ItemData.getItemsByCategory(this.props.match.params.category)
    .then(res=>{
      console.log('success' , res.data);
      this.setState({
        data_loaded: true,
        data: res.data.items,
      });

    }
    ,err=>{
      console.log(err);
    })
  }
  render() {
    if(this.state.data_loaded){
    return (
      <div className="home-body" >
      <h2 align="center">Category: {this.props.match.params.category} </h2>
      <div className="home-body">
      {this.state.data.map((i)=>
        <div className="tile" style={{"width":"100%", "padding":"10px"}}
          key={i._id}>
          <Row>
            <Col xs={5}>
              <img src={i.images[0]} height="50px" alt="..."/>
            </Col>
            <Col xs={19} >
              Name: {i.name}
              <Divider type="vertical" />
              Price: £{i.price}
              <Divider type="vertical" />
              Brand: {i.brand}
              <Divider type="vertical" />
            </Col>
          </Row>
          
          
        </div>
        )}
      </div>
      



      </div>
    );}
    else{
      return(<div className="">Nout</div>)
    }  }
}

export {CategoryItems};