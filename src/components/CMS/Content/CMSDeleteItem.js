import React, { Component } from 'react';
import "../../../sass/delete.item.sass";
import {ItemData} from "../../../services";
import { Input, Button,  message } from "antd";
import {URL} from '../../../utils/constants';

const {Search} = Input;

export class CMSDeleteItem extends Component {
  constructor(props){
    super(props);
    this.state={
      url: '',
      hasLoaded: false,
      input:'',
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  handleDelete(){
    console.log(this.state.input)

    //delete from server
    ItemData.deleteItemById(this.state.input)
    .then(response=>{
      console.log('successfully deleted item');
      message.success('Successfully deleted item');
      this.setState({
        input:'',
        url: '',
        hasLoaded: false,
      })
    },error=>{
      message.error('Sorry could not delete the item');
      this.setState({
        hasLoaded:false,
      })
    })


  }



  handleSearch(value){
    console.log('here ',value);
    const url = URL + 'item/' + value;
    this.setState({
      url: url ,
      input: value,
      hasLoaded: true,
    })
    
  }
  render() {
    
    return (
      <div className="delete-item-container">
        <h2 align="center">Delete Item</h2>
     
        <Search
        placeholder="Enter product id"
        enterButton = "Search"
        size= "large"
        onSearch={value=>this.handleSearch(value)}
        name='search'
         />
         {this.state.hasLoaded &&
          <div className="">

         <Button 
         style={{marginTop:'20px', float:'right'}} 
         type='primary' size='large'
         onClick={this.handleDelete}
         >
          Delete</Button>
           
         <iframe title="frame-for-item" className="page-display-box" src={this.state.url}></iframe>
         </div>
       }
        </div>

    
    );
  }
}
