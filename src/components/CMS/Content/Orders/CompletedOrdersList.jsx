import React, { Component } from "react";
import {OrderMainPage} from './OrderMainPage';
import {OrderData} from "../../../../services";

class CompletedOrdersList extends Component {
	constructor(props){
	    super(props);
	    this.state={
	      order_list:[],
	      data_loaded: false,

	      
	    };
	}
	componentDidMount(){
	    OrderData.getCompletedOrdersList()
	    .then(res=>{
	          this.setState({
	            order_list: res.data.list,
	            data_loaded: true,

	          })
	        }
	    ,error=>{
	        console.log("Error fetching data.\n", error);
	    });
	}
	
	render() {
		if(this.state.data_loaded){

		return (
			<div className="home-body">
				<OrderMainPage
					title="Completed Orders"
					order_list={this.state.order_list}
					order_completed= "YES"
				/>
			</div>
		);
		}
		else{
			return(
				<div className="home-body">Error Loading Data</div>
				)
		}
	}
}
export { CompletedOrdersList };
