import React from 'react';
import {Link} from 'react-router-dom';

import {OrderData} from "../../../../services";


import {Table} from 'antd';

import {OrderTable} from './OrderTable';
import "../../../../sass/orders.sass";


class OrderMainPage extends React.Component {
    constructor(props){
        super(props);
        this.state={
          order_list: this.props.order_list,
        }
        
    }
        
    render() {
      const {order_list,order_completed} = this.state;
        return (
            <div className="home-body">
            <div className="title">{this.props.title}</div>
            <div className="home-body">
            {(order_list ) ? (
              <OrderTable order_list={order_list}
               />
              )
            :(<div className="home-body">
              <div className="title">No Data to Display</div></div>)
            
            }
            </div>
            
            </div>
        )
    }
}
export {OrderMainPage};