import React, { Component } from "react";
import { Link } from "react-router-dom";

import { Table, Button } from "antd";
import "../../../../sass/home.sass";

class OrderTable extends Component {
	constructor(props) {
		super(props);
	}
	render() {
		var dataSource = [];
		const data = this.props.order_list;
		for (let i = 0; i < data.length; i++) {
			var date = new Date(data[i].order_date);
			let temp = {
				key: i,
				order_id: data[i]._id,
				username: data[i].username,
				user_id: data[i].user_id,
				order_date: date.toString().slice(3, 24),
				order_completed: data[i].order_completed,
			};
			dataSource.push(temp);
		}

		const columns = [
			{
				title: "Order Id",
				dataIndex: "order_id",
				key: "order_id",
				render: (text, record) => (
					<Link
						className="link-style"
						to={{
							pathname: "order/" + record.order_id,
							
						}}
					>
						{text}
					</Link>
				),
			},
			{
				title: "Name",
				dataIndex: "username",
				key: "username",
			},
			{
				title: "User ID",
				dataIndex: "user_id",
				key: "user_id",
			},
			{
				title: "Date Ordered",
				dataIndex: "order_date",
				key: "order_date",
			},
			{
				title: "Order Completed",
				dataIndex: "order_completed",
				key: "order_completed",
				render: (text, record) => (
					<Button 
					type = { (record.order_completed === 'YES')? 'primary' : 'danger'}
					
					>
						{text}
					</Button>
				),
			},
		];
		// const state={
		// rowSelection:{ onSelect:()=>{console.log('hello')}}
		// }
		return (
			<div className="home-body">
				<Table dataSource={dataSource} columns={columns} />
			</div>
		);
	}
}

export { OrderTable };
