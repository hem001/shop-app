import React, { Component } from "react";

import { OrderData } from "../../../../services";

import { OrderItemCard } from "./OrderItemCard";
import { Row, Col, Card, Button, message, Popconfirm } from "antd";
import "../../../../sass/orders.sass";

class OrderItemPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order: {},
      data_loaded: false,
      order_completed: '',
      status: [], //status of each items, if in basket or not
      l: 0,
    };
    this.orderCompleted = this.orderCompleted.bind(this);
    this.undoOrderCompleted = this.undoOrderCompleted.bind(this);
    this.addOrderToBasket = this.addOrderToBasket.bind(this);
  }
  addOrderToBasket(j) {
    let status = this.state.status;
    status[j]= "YES";
    this.setState({
      status,
    })
  }
  
  undoOrderCompleted() {
    const order_id = this.props.match.params.order_id;
    OrderData.undoCompleteOrder(order_id).then(
      (res) => {
        message.success("Successfully completed the task");
        let status = new Array(this.state.l).fill("NO");
        this.setState({
          order_completed: "NO",
          status,
        });
      },
      (err) => {
        message.error("Sorry. There was an error completing the task");
      }
    );
  }

  orderCompleted() {
    //check if all items in the orderlist have been put in basket to dispatch
    if(this.state.status.find(e=> e === 'NO')){
      message.error('You have missed some items.');
      return;
    }
    
    const order_id = this.props.match.params.order_id;

    OrderData.completeOrder(order_id).then(
      (res) => {
        message.success("Successfully completed the task");
        const status = new Array(this.state.l).fill("YES");
        this.setState({
          order_completed: "YES",
          status,
        });
      },
      (err) => {
        console.log("Error ", err);
        message.error("Sorry. There was an error completing the task");
      }
    );
  }

  componentDidMount() {
    
    OrderData.getOrderById(this.props.match.params.order_id).then(
      (res) => {
        const l = res.data.order.shopping_cart.length;
        const s = res.data.order.order_completed;
        var status = new Array(l).fill(s);
        this.setState({
          order: res.data.order,
          data_loaded: true,
          order_completed: s,
          l,
          status,
        });
        // console.log(' obj ',res.data.order);
      },
      (error) => {
        console.log("Error", error);
      }
    );
  }

  render() {
    if (this.state.data_loaded) {
      return (
        <div className="home-body">
          <div className="title">Ordered By: {this.state.order.username} </div>
          <div className="home-body">
            <Row>
              <Col sm={12}>
                <Row>
                  <Col sm={8} xs={24}>
                    Username :
                  </Col>
                  <Col sm={16} xs={24}>
                    {this.state.order.username}
                  </Col>
                </Row>
                <Row>
                  <Col sm={8}>Order Id :</Col>
                  <Col sm={16}>{this.state.order._id}</Col>
                </Row>
              </Col>
              <Col sm={12}>
                <Card
                  title="Billing Address"
                  style={{ width: "300px", margin: "auto" }}
                >
                  <span style={{ paddingLeft: "25px" }}>
                    <p>Address is:</p> Eu sed velit in voluptate. <br />
                    Lorem ipsum ut ex elit.
                    <br />
                  </span>
                </Card>
              </Col>
            </Row>
            <Row>
              <Col>
                <div className="home-body">
                  {this.state.order.shopping_cart.map((i, j) => (
                    <div className="order_item_card_body" key={j}>
                      <OrderItemCard item={i} />
                      <div className=""
                      style={{"textAlign":"right"}}
                      >
                      {
                        this.state.status[j] === "NO" ? (
                        
                        
                        <Button
                          type="primary"
                         onClick={()=>{this.addOrderToBasket(j)}}
                        >Add to Basket
                        </Button>
                      ) : (
                        <Button disabled={true}>
                          Added to Basket
                        </Button>
                      )}
                      </div>
                    </div>
                  ))}
                </div>
              </Col>
            </Row>
            <Row>
              <Col xs={12}>
                <Button type="danger">Cancel</Button>
              </Col>
              <Col xs={12} align="right">
                {this.state.order_completed === "NO" ? (
                  <Popconfirm
                    title="Are you sure?"
                    onConfirm={this.orderCompleted}
                  >
                    <Button type="primary">Complete Order</Button>
                  </Popconfirm>
                ) : (
                  <Popconfirm
                    title="Are you sure?"
                    onConfirm={this.undoOrderCompleted}
                  >
                    <Button type="primary">Undo completed Order</Button>
                  </Popconfirm>
                )}
              </Col>
            </Row>
          </div>
        </div>
      );
    } else {
      return (
        <div className="">
          <p>nout</p>
        </div>
      );
    }
  }
}

export { OrderItemPage };
