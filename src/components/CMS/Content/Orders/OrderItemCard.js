import React, { Component } from 'react';
import {ItemData} from "../../../../services";

import {Button, Card, Row, Col} from 'antd';
import '../../../../sass/orders.sass';


class OrderItemCard extends Component {
  constructor(props){
    super(props);
    this.state={
      data_loaded: false,
      data: {},
      added_to_basket: false,
      order_completed: "",
    }


  }
  

  componentDidMount(){
    this.setState({
      order_completed: this.props.order_completed
    });
    // console.log('here ', this.props)
    ItemData.getItemById(this.props.item._id)
    .then(res=>{
      // console.log('good',this.props.item.quantity);
      this.setState({
        data_loaded: true,
        item: {
          brand: res.data.item.brand,
          image: res.data.item.images[0],
          name: res.data.item.name,
          price: res.data.item.price,
          weight: res.data.item.weight,
          dimension: res.data.item.dimension,
        }

      })
    }
    ,error=>{
      console.log(error);
    });
  }


  render() {
    if(this.state.data_loaded){

    return (
      <React.Fragment>
        <Card
        className="order_item_card">
        <h1 align="center">Item: {this.state.item.name}</h1>
        <Row>
          <Col lg={6} xs={12} >
            <img src={this.state.item.image} alt="" width="100%" />
          </Col>

          <Col lg={18} xs={12} >
            <div className="order_item_content">
            <Row>
              <Col xs={12}>
                Brand:
              </Col>
              <Col xs={12}>
                {this.state.item.brand}
              </Col>
            </Row>

            <Row>
              <Col xs={12}>
                Price:
              </Col>
              <Col xs={12}>
                {this.state.item.price}
              </Col>
            </Row>

            <Row>
              <Col xs={12}>
                Weight:
              </Col>
              <Col xs={12}>
                {this.state.item.weight}
              </Col>
            </Row>

            <Row>
              <Col xs={12}>
                Dimension:
              </Col>
              <Col xs={12}>
                {this.state.item.dimension}
              </Col>
            </Row>

            <Row>
              <Col xs={12}>
                Quantity:
              </Col>
              <Col xs={12}>
                {this.props.item.quantity}
              </Col>
            </Row>

            <Row>
            
            </Row>


            </div>
            
          </Col>
        </Row>
        
        </Card>
      </React.Fragment>
    );
  }
  else{
    return(
      <h1>Data not found.</h1>)
  }
  }
}

export {OrderItemCard};