import React, { Component } from 'react';
import {OrderMainPage} from './OrderMainPage';
import {OrderData} from "../../../../services";

class AllOrdersList extends Component {
	constructor(props){
	    super(props);
	    this.state={
	      order_list:[],
	      data_loaded: false,
	    };
	}
	componentDidMount(){
	    OrderData.getOrderList()
	    .then(response=>{
	    	console.log(response.data.orderList)
	          this.setState({
	            order_list: response.data.orderList,
	            data_loaded:true,
	          })
	        }
	    ,error=>{
	        console.log("Error fetching data.\n", error);
	    });
	}
	
	render() {
		if(this.state.data_loaded){
		return (
			<div className="home-body">
				<OrderMainPage 
				title="All Orders"
				order_list = {this.state.order_list}
				
				/>
			</div>
		);
	}else{
		return(
			<div className="home-body">Error Loading Data</div>
			)
		
	}
	}
}
export {AllOrdersList};