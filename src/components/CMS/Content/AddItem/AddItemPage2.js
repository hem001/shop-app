import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {  Form, Input, Button, message } from "antd";
import { onInputChange } from "../../../../utils/componentUtils";
import { cmsActions } from "../../../../redux/actions/index";
import "../../../../sass/add.item.sass";

class AddItemPage2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      brand: 'brand',
      dimension:'dimension',
      weight:'weight',
      color:'color',
      
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }
 onCancel(){
  this.props.dispatch(cmsActions.cms_clearItem());
  this.props.history.push('/cms-dashboard/addItem');
 }
  handleSubmit(){
    if(!this.state.brand || 
       !this.state.dimension ||
       !this.state.weight ||
       !this.state.color){
      message.error('Please enter all fields');

    }else{
      console.log("cms_data ", this.props.dispatch(cmsActions.cms_getItem()));
      this.props.dispatch(cmsActions.cms_saveItem(this.state));
      this.props.history.push('/cms-dashboard/addItemPage3');

    }
  }
  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 }
      },
      labelAlign: "left"
    };
    return (
      <React.Fragment>
        <div className="add-item-container">
          <h2 align="center">Item Details Page2 </h2>
          <div className="add-item-card-body">
            <Form {...formItemLayout} >
              <Form.Item label="Brand name">
                <Input
                  placeholder="Brand Name"
                  onChange={onInputChange.bind(this)}
                  value={this.state.brand}
                  name="brand"
                  type="text"
                />
              </Form.Item>
              <Form.Item label="Dimension">
                <Input
                  placeholder="Dimension AxBxC cm"
                  onChange={onInputChange.bind(this)}
                  value={this.state.dimension}
                  name="dimension"
                  type="text"
                />
              </Form.Item>
              <Form.Item label="Weight">
                <Input
                  placeholder="Weight in gm"
                  onChange={onInputChange.bind(this)}
                  value={this.state.weight}
                  name="weight"
                  type="text"
                />
              </Form.Item>
              <Form.Item label="Color">
                <Input
                  placeholder="Color"
                  onChange={onInputChange.bind(this)}
                  value={this.state.color}
                  name="color"
                  type="text"
                />
              </Form.Item>

            </Form>
          </div>
          <Button
            type="primary"
            className="add-item-form-button"
            onClick={this.onCancel}
          >
            Cancel
          </Button>

            <Button type="primary" htmlType="submit"
            onClick={this.handleSubmit}
            className="add-item-form-button" >
              Next
            </Button>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    cms_item_data: state.cms.cms_item_data
  };
};

const con = connect(mapStateToProps, null)(withRouter(AddItemPage2));

export { con as AddItemPage2 };
