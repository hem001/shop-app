import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {  Input, Button,  message, Tooltip } from "antd";
import { cmsActions } from "../../../../redux/actions/index";
import {ItemData} from "../../../../services";

import "../../../../sass/add.item.sass";


class AddItemPage3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disable_submit: false,
      data: [["a", "a"],["b","b"],["c","c"]]
    };
    this.handleAddButton = this.handleAddButton.bind(this);
    this.handleRemoveButton = this.handleRemoveButton.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleOnBlur = this.handleOnBlur.bind(this);
  }

  handleOnBlur= k=> e=>{
    //check if the first input item already exists in the preceeding fields
    var l =  this.state.data.length
    if(l > 1){
      for(var i =0; i<k;i++){
        if(this.state.data[i][0] === e.target.value){
          message.error('Error, value already entered before');
          console.log('Errro');
          this.setState({
            disable_submit: true,
          })
          return;
        }
      }
      this.setState({
        disable_submit: false,
      })
    }
  }

  onSubmit(){
    var item_data ={};
    // console.log(this.props.cms_item_data);
    
    this.props.cms_item_data.map((i,j)=>{
      item_data = Object.assign(item_data, i);
    })

    //for the data collected in page 3.
    //check last elements of this.state.data[]
    if(this.state.data.length){
      const last = this.state.data.length-1;

      if(this.state.data[last][0] === '' || this.state.data[last][1] === ''){
        const ndata = this.state.data;
        ndata.splice(last,1);
        this.setState({
          data : ndata
        })
      }
      console.log('data is ', this.state.data);
      this.state.data.map(i=>
        {
          //check if key is not already exists
          if(item_data[i[0]]){
              message.error('Key already exists. Please change it.');
              return;
          }
          // item_data[i[0]] = i[1];

        }
        )


      item_data["other_details"] = this.state.data;

    }

    ItemData.addItemData(item_data)
    .then(response =>{
      message.success('Successfully stored data to server');
      console.log('Successfully posted data to server');
      this.props.dispatch(cmsActions.cms_clearItem());
      this.props.history.push('/cms-dashboard');
    }
    ,error=>{
      console.log('Error posting data', error);
      message.error('Error posting data to server');
    });
    
    console.log('item data is ', item_data);
  }

  onCancel(){
    this.props.dispatch(cmsActions.cms_clearItem());
    this.props.history.push('/cms-dashboard');
  }
  onInputChange = i => e => {
    let data = [...this.state.data];
    if (e.target.name === "first") {
      data[i][0] = e.target.value;
    } else {
      data[i][1] = e.target.value;
    }
    this.setState({
      data
    });
  };
  handleRemoveButton(e) {
    const i = parseInt(e.target.name);
    const ndata = this.state.data;
    ndata.splice(i, 1);
    this.setState({
      data: ndata
    });

    console.log("removed ", i, this.state.data);
  }
  handleAddButton(e) {
    // const i = parseInt(e.target.name);
    const last = this.state.data.length-1;

    //check if fields are empty. if empty do nothing
    if (this.state.data.length) {
      if((this.state.data[last][0] === "" || this.state.data[last][1] === "") ) {
      message.error("Please enter both fields.");
      return;
    }}

    this.setState({
      data: [...this.state.data, ["", ""]]
    });

    console.log("added ", this.state.data);
  }

  render() {
    return (
      <React.Fragment>
        <div className="add-item-container">
          <h2 align="center">Add more details </h2>
          <div className="add-item-card-body">
            
            {this.state.data.map((i, j) => (
              <InputPair
                key={j}
                handleChange={this.onInputChange(j)}
                handleAddButton={this.handleAddButton}
                name={j}
                handleRemoveButton={this.handleRemoveButton}
                handleOnBlur ={this.handleOnBlur(j)}
                data={this.state.data[j]}
              />
            ))}
            <Tooltip title="Add more details">
              <Button icon="plus" type="primary" onClick={this.handleAddButton}
              name ="add">
                Add
              </Button>
            </Tooltip>
          </div>
          
            <Button 
            type="primary" 
            className="add-item-form-button"
            onClick={this.onCancel} >
              Cancel
            </Button>
          
            <Button type="primary" className="add-item-form-button"
            onClick={this.onSubmit}
            disabled = {this.state.disable_submit} >
              Submit
            </Button>
        </div>
      </React.Fragment>
    );
  }
}

const InputPair = ({
  data,
  name,
  handleChange,
  handleRemoveButton,
  handleOnBlur,
}) => {
  return (
    <div className="input-pair-style">
      <Input
        style={{ width: "30%", margin: "5px" }}
        placeholder="Item Detail"
        onChange={handleChange}
        onBlur = {handleOnBlur}
        value={data[0]}
        name="first"
        type="text"
      />

      <Input
        style={{ width: "40%", margin: "5px" }}
        placeholder="Description"
        onChange={handleChange}
        value={data[1]}
        name="second"
        type="text"
      />
      <Tooltip title="Remove details">
      <Button
        icon="minus"
        type="primary"
        style={{ margin: "5px" }}
        name={name}
        onClick={handleRemoveButton}
      ></Button>
      </Tooltip>
    </div>
  );
};

const mapStateToProps = state =>{
  return {
    cms_item_data: state.cms.cms_item_data,
  };
};

const con = connect(mapStateToProps, null)(withRouter(AddItemPage3));

export {con as AddItemPage3}