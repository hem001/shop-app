import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import "../../../../sass/add.item.sass";
import { Icon, Form, Input, Button, Select, message } from "antd";
import { onInputChange } from "../../../../utils/componentUtils";
import { cmsActions } from "../../../../redux/actions/";

const { TextArea } = Input;
const { Option } = Select;
class AddItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "q",
      price: "1",
      quantityToAdd: "1",
      category: "1",
      description: "1",
      images: [
        "https://cdn.pixabay.com/photo/2016/12/13/05/15/puppy-1903313__340.jpg",
        "https://www.cats.org.uk/media/2197/financial-assistance.jpg?width=1600"
      ]
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onAddImageButtonClicked = this.onAddImageButtonClicked.bind(this);
    this.onImageInputChange = this.onImageInputChange.bind(this);
    this.onSelectChange = this.onSelectChange.bind(this);
  }

  onSelectChange(value) {
    this.setState({
      category: value
    });
  }

  onAddImageButtonClicked(e) {
    e.preventDefault();
    //prevent adding empty inputs by Add Image button
    if (this.state.images[this.state.images.length - 1] === "") {
      return;
    }

    console.log("clicked", this.state.images[this.state.images.length - 1]);
    let images = this.state.images.concat([""]);
    this.setState({
      images: images
    });
  }
  onImageInputChange = i => e => {
    let images = [...this.state.images];
    if (e.target.value === "") {
      return;
    }
    images[i] = e.target.value;
    this.setState({
      images
    });
  };

  onCancel() {
    //clear the state variables
    this.setState({
      name: "",
      price: "",
      quantityToAdd: "",
      category: "Category",
      description: "",
      images: [""]
    });
  }

  handleSubmit() {
    if (
      !this.state.name ||
      !this.state.price ||
      !this.state.quantityToAdd ||
      !this.state.description ||
      (this.state.images.length === 1 && this.state.images[0] === "")
    ) {
      message.error("Please enter all fields");
    } else if (
      parseInt(this.state.price) < 0 ||
      parseInt(this.state.quantityToAdd) < 0
    ) {
      console.log("Numerical values should be greater than zero");
      message.error("Please enter numerical values more than 0.");
    } else if (this.state.category === "Category") {
      message.error("Please select a category");
    } else {
      //check last images[] is not empty
      if (this.state.images[this.state.images.length - 1] === "") {
        this.state.images.splice(this.state.images.length - 1, 1);
      }

      this.props.dispatch(cmsActions.cms_clearItem());
      console.log('state is reset');
      this.props.dispatch(cmsActions.cms_saveItem(this.state));

      console.log("here props.cms_item_data ", this.props.cms_item_data);
      this.props.history.push("/cms-dashboard/addItemPage2");

      // userData.addItemData(this.state)
      // .then(response =>{
      //  //clear the state variables
      //  this.setState({
      //    name:'',
      //    price:'',
      //    quantityToAdd:'',
      //    category:'',
      //    description:'',
      //    images:[],
      //  });
      // }
      // ,error=>{
      //  console.log('Error posting data', error);
      // })
    }
  }

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 18 }
      },
      labelAlign: "left"
    };
    return (
      <div className="add-item-container">
        <h2 align="center">Item Details</h2>

        <div className="add-item-card-body">
          <Form {...formItemLayout}>
            <Form.Item label="Name">
              <Input
                prefix={<Icon type="user" style={{ color: "gray" }}></Icon>}
                placeholder="Name"
                onChange={onInputChange.bind(this)}
                value={this.state.name}
                name="name"
                type="text"
              />
            </Form.Item>
            <Form.Item label="Price">
              <Input
                prefix={<Icon type="pound" style={{ color: "gray" }} />}
                placeholder="Price"
                onChange={onInputChange.bind(this)}
                value={this.state.price}
                name="price"
                type="number"
                min={1}
              />
            </Form.Item>
            <Form.Item label="QuantityToAdd">
              <Input
                prefix={<Icon type="user" style={{ color: "gray" }} />}
                placeholder="Quantity to Add"
                onChange={onInputChange.bind(this)}
                value={this.state.quantityToAdd}
                name="quantityToAdd"
                type="number"
                min={1}
              />
            </Form.Item>
            <Form.Item label="Category">
              <Select
                defaultValue="Category"
                style={{ width: "50%" }}
                onSelect={this.onSelectChange}
                value={this.state.category}
                name="category"
              >
                <Option value="category">Category</Option>
                <Option value="1">1</Option>
                <Option value="2">2</Option>
                <Option value="3">3</Option>
                <Option value="4">4</Option>
                <Option value="5">5</Option>
              </Select>
            </Form.Item>
            <Form.Item label="Images">
              {this.state.images.map((i, j) => (
                <Input
                  key={j}
                  prefix={<Icon type="picture" style={{ color: "gray" }} />}
                  placeholder="Image"
                  onChange={this.onImageInputChange(j)}
                  value={i}
                  type="text"
                />
              ))}
              <Button onClick={this.onAddImageButtonClicked}>Add Image</Button>
            </Form.Item>
            <Form.Item label="Description">
              <TextArea
                rows={5}
                prefix={<Icon type="user" style={{ color: "gray" }} />}
                placeholder="Description"
                onChange={onInputChange.bind(this)}
                value={this.state.description}
                name="description"
                type="text"
              />
            </Form.Item>
          </Form>

          
        </div>
        <Button
          type="primary"
          className="add-item-form-button"
          onClick={this.onCancel}
        >
          Cancel
        </Button>

        <Button
          type="primary"
          onClick={this.handleSubmit}
          className="add-item-form-button"
        >
          Next
        </Button>
        
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    cms_item_data: state.cms.cms_item_data
  };
};
const con = connect(mapStateToProps, null)(withRouter(AddItem));
export { con as AddItem };
