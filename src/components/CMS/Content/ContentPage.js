import React, { Component } from "react";
import { connect } from "react-redux";

import { AddItem } from "./AddItem";

class ContentPage extends Component {
  render() {
    return (
      <React.Fragment>
        {this.props.contentPageOption === "ADD-ITEM" ? <AddItem /> : null}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    contentPageOption: state.contentPageOption
  };
};

const con = connect(mapStateToProps, null)(ContentPage);
export { con as ContentPage };
