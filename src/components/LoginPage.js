import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import {Auth} from "../services";
import  { authActions }  from "../redux/actions/";
import { connect } from "react-redux";

import "../sass/login-page.sass";

import { onInputChange } from "../utils/componentUtils";
import { Card, Icon, Form, Input, Button, message, Alert } from "antd";
import { CMSDASHBOARD_URL } from "../utils/constants";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      admin_user: "false",
      email: "",
      pwd: "",
      error_message: ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit() {

    if (!this.state.email || !this.state.pwd) {
      this.setState({
        error_message: "please enter both fields"
      });
      return;
    }

    var user = {
      email: this.state.email,
      password: this.state.pwd,
      //this.props.admin_user passed from Router
      admin_user: this.props.admin_user ? true : false
    };

    // if (this.props.admin_user) {
    //   user.admin_user = "true";
    // }

    Auth.login(user).then(
      res => {
        localStorage.setItem("app-token", res.data.token);
        const data = {
          username : res.data.username,
          _id : res.data._id,
        }
        
        if (this.props.admin_user) {
          console.log('admin user')
          localStorage.setItem("adminLoggedIn", true);
          this.props.dispatch(authActions.loginAdmin(data));
          this.props.history.push(CMSDASHBOARD_URL);
        } else {
          localStorage.setItem("userLoggedIn", res.data.username);
          this.props.dispatch(authActions.login(data));
          if(this.props.shopping_cart.length){
            this.props.history.push('/shopping-cart');
          }
          message.success(res.data.message);

        }
      },
      error => {
        this.setState({
          error_message: "You have entered wrong details"
        });
      }
    );
    this.setState({
      email: "",
      pwd: "",
      error_message: ""
    });
  }

  render() {
    const {isUserLoggedIn} = this.props;
       
    return (
      <div>
        <Card className="card-style">
          <div className="card-header">
            <h2 align="center">Login</h2>
          </div>
          <div className="card-body">
            <Form
              onKeyPress={e => {
                if (e.key === "Enter") {
                  this.handleSubmit();
                }
              }}
              className="login-form"
            >
              {this.state.error_message ? (
                <Alert message={this.state.error_message} type="error" />
              ) : (
                <React.Fragment></React.Fragment>
              )}
              <Form.Item>
                <Input
                  prefix={
                    <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  placeholder="Email"
                  onChange={onInputChange.bind(this)}
                  value={this.state.email}
                  name="email"
                />
              </Form.Item>
              <Form.Item>
                <Input
                  prefix={
                    <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                  }
                  type="password"
                  placeholder="Password"
                  onChange={onInputChange.bind(this)}
                  value={this.state.pwd}
                  name="pwd"
                />
              </Form.Item>
              <br />

              <Form.Item>
                <Button
                  type="primary"
                  onClick={this.handleSubmit}
                  block
                  className="login-form-button"
                >
                  Log in
                </Button>
                {isUserLoggedIn ?  (
                  <Link to="/dashboard">
                    <Button type="primary" ghost>
                      Register Now
                    </Button>
                  </Link>
                ):(
                  null
                ) }
              </Form.Item>
            </Form>
          </div>
        </Card>
      </div>
    );
  }
}

function mapStateToProps (state) {
  return  {
    isUserLoggedIn: state.auth.isUserLoggedIn ,
    shopping_cart: state.cart.shopping_cart,
  }
  
};

const connectedLogin = connect(mapStateToProps, null)(withRouter(Login));

export { connectedLogin as Login };
