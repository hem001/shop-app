import React, { Component } from "react";
// import { Link } from "react-router-dom";

import {Auth} from "../services";
import {Card, Icon, Form, Input,Button,Alert } from "antd";



class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      error_message: ""
    };
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }
  onFormSubmit() {
    if (!Auth.isLoggedIn()) {
      this.setState({
        error_message: "Please log in to register a new user"
      });
      console.log('not logged in');
      return;
    } else if (!this.state.name || !this.state.email || !this.state.password) {
      this.setState({
        error_message: "Please enter all fields"
      });
      return;
    }

    var user = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password
    };
    Auth.register(user).then(
      function(res) {
        localStorage.setItem("app-token", "");
        localStorage.setItem("userLoggedIn", res.data.username);
      },
      err => {
        console.log("unable to reigster");
      }
    );

    this.setState({
      name: "",
      email: "",
      password: ""
    });
  }
  onInputChange(e) {
    var val = e.target.value;
    this.setState({
      [e.target.name]: val
    });
  }
  render() {
    var ErrorMessage = () => {
      if(this.state.error_message){
      return<Alert type='error'
      message= {this.state.error_message}></Alert>
    }else{
      return<React.Fragment></React.Fragment>
    }

    };

    return (
      <div>
        <Card className="card-style">
        <div className="card-header">
            <h2 align="center">Register</h2>
        </div>
          <div className="card-body">
          <ErrorMessage/>
          <Form onKeyPress={e=>{if(e.key==='Enter'){this.onFormSubmit();}}} className="login-form">
                  <Form.Item>
                      <Input
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Name"
                        onChange={this.onInputChange}
                        value={this.state.name}
                        name="name"
                      />
                  </Form.Item>
                  <Form.Item>
                      <Input
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Email"
                        onChange={this.onInputChange}
                        value={this.state.email}
                        name="email"
                      />
                  </Form.Item>
                  <Form.Item>
                      <Input
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password"
                        placeholder="Password"
                        onChange={this.onInputChange}
                        value={this.state.password}
                        name="password"
                      />
                  </Form.Item>
                  <br/>

                  <Form.Item>
                    <Button type="primary" onClick= {this.onFormSubmit} block className="login-form-button">
                      Submit
                    </Button>
                  </Form.Item>
                </Form>
          </div>
        </Card>
      </div>
    );
  }
}

export default Register;
