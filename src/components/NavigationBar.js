import React, { Component } from "react";
import { connect } from "react-redux";
import { authActions } from "../redux/actions/";
import { Link, withRouter } from "react-router-dom";
import { Menu, Icon, Dropdown, message } from "antd";
import "../sass/navigation.sass";
import { Auth } from "../services";
import { ItemData } from "../services";
const { SubMenu } = Menu;

class NavigationBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      show_modal: false,
      modal_message: "",
      data_loaded: false,
      categories_list: [],
    };
    this.clickedLogout = this.clickedLogout.bind(this);
  }

  componentDidMount() {
    ItemData.getCategoriesList().then(
      (res) => {
        this.setState({
          categories_list: res.data.categories,
          data_loaded: true,
        });
      },
      (err) => {
        console.log(err);
      }
    );
  }
  clickedLogout = () => {
    this.setState({
      show_modal: true,
      modal_message: "You have logged out!!!",
    });
    this.props.dispatch(authActions.logout());
    if (this.props.isAdminLoggedIn) {
      this.props.dispatch(authActions.logoutAdmin());
    }
    Auth.logout();
    message.warning("You have logged out!!!");
  };

  render() {
    const { username } = this.props;
    // var cart_size = this.props.shopping_cart ? this.props.shopping_cart.length : 0;
    // const cart_size = this.props.shopping_cart.length;
    const LoginOut = () => {
      if (!Auth.isLoggedIn()) {
        return (
          <div className="">
            <Link to="/login">
              <Icon type="login" />
              Login
            </Link>
          </div>
        );
      } else {
        return (
          <Dropdown
            overlay={
              <Menu>
                <Menu.Item>
                  <p onClick={this.clickedLogout}>Logout</p>
                </Menu.Item>
              </Menu>
            }
          >
            <p>
              <Icon type="logout" />
              {username}
            </p>
          </Dropdown>
        );
      }
    };

    return (
      <React.Fragment>
        <Menu className="nav-style" mode="horizontal">
          <Menu.Item>
            <Link to="/">
              <Icon type="double-right" spin />
              <Icon type="setting" />
              Home
            </Link>
          </Menu.Item>
          <SubMenu
            style={{fontSize:"17px"}}
            title="Categories"
            onTitleClick={() => this.props.history.push("/categories")}
          >
          
            {this.state.data_loaded
              ? this.state.categories_list.map((i) => (
                  <Menu.Item key={i} style={{width:"300px",

                  }}>
                  <Link style={{fontSize:"14px"}} to={"/category/"+i} >
                    Category: {i} 
                  </Link>
                    </Menu.Item>
                ))
              : null}
              
          </SubMenu>

          <Menu.Item>
            <Link to="/about">
              <Icon type="setting" />
              About
            </Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/contact">
              <Icon type="setting" />
              Contact
            </Link>
          </Menu.Item>

          <Menu.Item style={{ float: "right" }}>
            <LoginOut />
          </Menu.Item>

          <Menu.Item style={{ float: "right" }}>
            <Link to="/shopping-cart">
              <Icon type="shopping-cart" style={{ fontSize: "20px" }} />
              {this.props.shopping_cart.length
                ? this.props.shopping_cart.length
                : ""}
            </Link>
          </Menu.Item>
        </Menu>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    username: state.auth.username,
    isUserLoggedIn: state.auth.isUserLoggedIn,
    isAdminLoggedIn: state.auth.isAdminLoggedIn,
    shopping_cart: state.cart.shopping_cart,
  };
};
const connectedNav = connect(mapStateToProps, null)(withRouter(NavigationBar));
export { connectedNav as NavigationBar };
