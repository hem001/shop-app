import React, { Component } from "react";

import { Card, Form, Icon, Input, Button } from "antd";

import "../sass/contact.sass";
const { TextArea } = Input;


class Contact extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: "",
			email: "",
			body: ""
		};
		this.onInputChange = this.onInputChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}
	onInputChange(e) {
		this.setState({
			[e.target.name]: e.target.value
		});
	}
	onSubmit() {
		console.log(this.state);
		this.setState({
			name: "",
			email: "",
			body: ""
		});
	}

	render() {
		return (
			<div>
				<Card className="card-style">
				<div className="card-header">
					<h2 align="center">Contact Us</h2>
				</div>
					<div className="card-body">
						<Form>
							<Form.Item>
							<Input
							prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
							placeholder="Name" 
							name="name"
							value={this.state.name}
							onChange = {this.onInputChange}

							/>
						
							</Form.Item>
							<Form.Item>
							<Input
							prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
							placeholder="Email address" 
							name="email"
							value={this.state.email}
							onChange = {this.onInputChange}
							/>
							
							</Form.Item>
							<Form.Item>
							<TextArea
							placeholder="Body" rows={5}
							name="body"
							value={this.state.body}
							onChange = {this.onInputChange}
							/>
							
							</Form.Item>
							<Form.Item>
								<Button block type="primary" onClick={this.onSubmit}>Submit</Button>
							</Form.Item>

						</Form>
					</div>
				</Card>
        
        
        
        
			</div>
		);
	}
}
export default Contact;
