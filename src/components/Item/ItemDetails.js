import React, { Component } from "react";
import { onInputChange } from "../../utils/componentUtils";
import {ItemData} from "../../services";

import "../../sass/item.sass";

import {
  Col,
  Tabs,
  Progress,
  Table,
  Comment,
  Avatar,
  Rate,
  message,
  Form,
  Input,
  Button
} from "antd";

// import { API } from "../../utils/constants";
// import axios from "axios";
const { TabPane } = Tabs;
const { TextArea } = Input;

export class ItemDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      add_review: false,
      review_added: false,
      submitted_by: "",
      comment: "",
      stars: 1,
      comments: this.props.item.comments,
      item_ratings: this.props.item.item_ratings,
    };
    this.addReviewButtonPressed = this.addReviewButtonPressed.bind(this);
    this.onCancelReview = this.onCancelReview.bind(this);
    this.onReviewAdded = this.onReviewAdded.bind(this);
    this.submitComment = this.submitComment.bind(this);
    this.onRateChange = this.onRateChange.bind(this);
  }


  submitComment() {
    console.log("submit comment", this.props.item.comments);
    if (!this.state.submitted_by || !this.state.comment) {
      message.error("Please enter both fields");
      return;
    }
    var ratings = this.state.item_ratings;
    ratings[this.state.stars-1]= ratings[this.state.stars-1]+1;
    
    ItemData
      .addReviewById({
        submitted_by: this.state.submitted_by,
        comment: this.state.comment,
        _id: this.props.item._id,
        stars: this.state.stars,
        item_ratings: ratings,
      })
      .then(
        response => {
          console.log("submitted successfully");
          var comments = this.state.comments;
          comments.push({
            submitted_by: this.state.submitted_by,
            comment: this.state.comment,
            _id: this.props.item._id,
            stars: this.state.stars
          }
          );
          
          message.success("Thank you for your Review");
         

          this.setState({
            review_added: true,
            comments,
            item_ratings: ratings,

          });
      console.log('push ', this.state.item_ratings);
        },
        error => {
          console.log("Error ");
          message.error("Sorry, data could not be uploaded.");
        }
      );
  }
  onRateChange(r) {
    this.setState({
      stars: r
    });
  }

  addReviewButtonPressed() {
    this.setState({
      add_review: true
    });
  }
  onCancelReview() {
    this.setState({
      add_review: false
    });
  }
  onReviewAdded() {
    this.setState({
      review_added: true
    });
  }
  render() {
    return (
      <React.Fragment>
        <div className="tabbed-details ">
          <Tabs className="item-container" defaultActiveKey="3">
            <TabPane tab="Details" key="1">
              <Details item={this.props.item} />
            </TabPane>

            <TabPane className="item-container" key="2" tab="Reviews">
              <AddReview
                addReviewButtonPressed={this.addReviewButtonPressed}
                onCancelReview={this.onCancelReview}
                submitComment={this.submitComment}
                onInputChange={onInputChange.bind(this)}
                onRateChange={this.onRateChange}
                state={this.state}
              />

              <Reviews 
              item={this.props.item} 
              Reviews = {this.state.comments}
              />
            </TabPane>

            <TabPane className="item-container" key="3" tab="Ratings">
              <Ratings item_ratings={this.state.item_ratings} />
            </TabPane>
          </Tabs>
        </div>
      </React.Fragment>
    );
  }
}

function Ratings({item_ratings}){
  //takes ration as an array of 5 numbers. First number corresponds to 5star last to 1 star.
  /*
  data should be in the following format
  const rating = [90, 3, 4, 3, 2];//1star.. 5stars
  */
  const rating = item_ratings;
  // const rating = [1,2,3,4,65];
  let total = rating.reduce((a, b) => a + b, 0);

  let ratingObject = [];
  for (var i = 5; i>0 ; i--) {
    ratingObject.push({
      key: i,
      stars: <Rate value={i} disabled />,
      bar: (
        <Progress percent={Number(((rating[i-1] * 100) / total).toFixed(1))} />
      ),
      rate: rating[i-1]
    });
  }

  const columns = [
    {
      dataIndex: "stars",
      key: "stars"
    },
    {
      dataIndex: "bar",
      key: "bar"
    },
    {
      dataIndex: "rate",
      key: "rate"
    }
  ];

  return (
    <div className="space-out">
      <h3>Customer Rating ( {total} Reviews )</h3>

      <Table
        dataSource={ratingObject}
        columns={columns}
        showHeader={false}
        pagination={false}
      />
    </div>
  );
}

//Component for Details TabPane
const Details = ({ item }) => {
  //takes details information as object and displays using map function.
  //data should be in the following format
  /*
  const details = {
  Brand: "The Brush Company",
  Dimension: "2cm x 5cm x 10cm",
  Weight: "5kg",
  Materials: "Bamboo"
  */
  const details = {};
  details["Brand"] = item.brand;
  details["Dimension"] = item.dimension;
  details["Weight"] = item.weight;
  details["Color"] = item.color;
  if (item.other_details) {
    Object.keys(item.other_details).forEach(key => {
      // const tmp = key.toUpperCase();
      details[key] = item.other_details[key];
    });
  }

  let detailsObject = [];
  let k = Object.keys(details);
  let v = Object.values(details);
  for (var i = 0; i < k.length; i++) {
    detailsObject.push({
      key: i,
      feature: k[i],
      description: v[i]
    });
  }
  const columns = [
    {
      key: "1",
      dataIndex: "feature"
    },
    {
      key: "2",
      dataIndex: "description"
    }
  ];

  return (
    <div className="item-container">
      {item.description}
      <h3 className="space-out">Item Specifications</h3>
      <Col md={24} lg={20}>
        <Table
          dataSource={detailsObject}
          columns={columns}
          pagination={{ pageSize: 5 }}
          bordered
          showHeader={false}
        />
      </Col>
    </div>
  );
};

//Component Reviews
function Reviews({item, Reviews}) {
  //reviews should be an object as following:
  // [{
  //   submitted_by: 'Happy man',
  //   comment: 'this is a good comment',
  //   stars: 3,
  // },
  // {...}]

  const reviews = Reviews;

  let reviewsObject = [];
  if (reviews.length) {
    for (var i = reviews.length-1; i>=0  ; i--) {
      reviewsObject.push({
        key: i,
        comment: (
          <Comment
            author={reviews[i].submitted_by}
            avatar={
              <Avatar
                src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                alt="Han Solo"
              />
            }
            content={
              <div className="">
                <Rate value={reviews[i].stars} disabled />
                <p>{reviews[i].comment}</p>
              </div>
            }
          />
        )
      });
    }
    const columns = [
      {
        dataIndex: "comment",
        key: "comment"
      }
    ];

    return (
      <div className="space-out">
        Lorem ipsum qui proident velit sit dolor in aliquip nostrud est
        cupidatat ullamco dolore veniam duis sint aliqua mollit excepteur mollit
        esse occaecat eu eiusmod in ut aute exercitation tempor.-- Comment 
        <Table
          dataSource={reviewsObject}
          columns={columns}
          pagination={{ pageSize: 3 }}
          showHeader={false}
        />
      </div>
    );
  } else{
    return (
      <div className="">
        <h1>No Reviews to Display</h1>
      </div>
    );
  }
}

function AddReview({
  addReviewButtonPressed,
  onCancelReview,
  submitComment,
  onInputChange,
  onRateChange,
  state
  }) {
  //component to add Review

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 6 }
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 18 }
    },
    labelAlign: "left"
  };

  if (!state.add_review && !state.review_added) {
    return (
      <Button type="primary" onClick={addReviewButtonPressed}
       >
        Add Review
      </Button>
    );
  } else if (state.add_review && !state.review_added) {
    return (
      <div className="add-review-style">
        <Form {...formItemLayout}>
          <Form.Item label="Rate">
            <Rate value={state.stars} onChange={onRateChange} />
          </Form.Item>
          <Form.Item label="Name">
            <Input
              placeholder="Name"
              value={state.submitted_by}
              name="submitted_by"
              onChange={onInputChange}
            />
          </Form.Item>
          <Form.Item label="Comment">
            <TextArea
              placeholder="Comment"
              value={state.comment}
              name="comment"
              onChange={onInputChange}
              rows={4}
            />
          </Form.Item>

          <Form.Item>
            <Button
              type="primary"
              className="add-item-form-button"
              onClick={onCancelReview}
            >
              Cancel
            </Button>

            <Button
              type="primary"
              className="add-item-form-button"
              onClick={submitComment}
            >
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  } else {
    return null;
  }
}
