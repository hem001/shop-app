import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { Row, Col, Button, InputNumber, Form, Modal } from "antd";
import { cartActions } from "../../redux/actions";
import "../../sass/item.sass";


class ItemShortDescription extends Component {
  constructor(props) {

    super(props);
    this.state = {
      item: {},
      quantity: 1,
    };
    this.showConfirm = this.showConfirm.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  addToBasket() {
    // console.log("no of items", this.props.item._id);
    this.props.dispatch(cartActions.addToShoppingCart({
      _id: this.props.item._id,
      quantity: this.state.quantity,
      
    }));
  }
  onChange(value) {
    this.setState({
      quantity: value
    });
  }

  showConfirm() {
    Modal.success({
      title: "Item Added to the shopping basket?",
      okText: 'OK',
      onOk: this.addToBasket.bind(this),
    });
  }
  render() {
    return (
      <div className="item-description-container" >
				<h2>Product Description:</h2>
				<Row >
					<Col sm={18}>
						<h4>Price:</h4>
					</Col>
					<Col sm={6}>
						<h4>£{this.props.item.price}</h4>
					</Col>
				</Row>
				<Row >
					<Form >
						<Col xs={12}>
							<Form.Item label="Quantity: ">
								<InputNumber
									defaultValue={1}
									name="quantity"
									min={1}
									onChange={this.onChange}
                  step={1}
								/>
							</Form.Item>
						</Col>

						<Col xs={12}>
							<Form.Item>
								<Button
									type="primary"
									onClick={this.showConfirm}
                  block
								>
									Add To Basket
								</Button>
							</Form.Item>
              <Form.Item>
              <Link to="/shopping-cart">
              <Button
                type="primary"
                block
              >
                Buy Now
              </Button>
              </Link>
              </Form.Item>
						</Col>
					</Form>
				</Row>
				
				<Row>
					<Col>
						<h3>Description:</h3>
						<div className="">{this.props.item.description}</div>
					</Col>
				</Row>

			</div>
    );
  }
}

const con = connect(null, null)(ItemShortDescription);
export { con as ItemShortDescription };