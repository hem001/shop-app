import React, { Component } from "react";
import { connect } from "react-redux";

import { Row, Col } from "antd";
import "../../sass/item.sass";

import { ItemCarousel } from "./ItemCarousel";
import { ItemShortDescription } from "./ItemShortDescription";
import { ItemDetails } from "./ItemDetails";
import { ItemData } from "../../services";

class ItemPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: {},
      hasLoaded: false,
      notFound: false,
      error_message: "",
    };
    this.getData = this.getData.bind(this);
  }
  componentDidMount() {
    this.getData();
  }
  getData() {
    ItemData.getItemById(this.props.match.params.item_id).then(
      (response) => {
        this.setState({
          item: response.data.item,
          hasLoaded: true,
          notFound: false,
        });
      },
      (error) => {
        console.log("Error ", error.message);
        this.setState({
          hasLoaded: true,
          notFound: true,
          error_message: "Error: Item no Found.",
        });
      }
    );
  }
  render() {
    if (!this.state.hasLoaded) {
      return <div className="">LOADING....</div>;
    } else if (this.state.notFound) {
      return <h1>{this.state.error_message}</h1>;
    } else {
      return (
        <React.Fragment>
          <h1 align="center">{this.state.item.name} </h1>
          <Row>
            <Col xs={24} sm={12} md={12} lg={12}>
              <ItemCarousel
                images={this.state.item.images}
                name={this.state.item.name}
              />
            </Col>
            <Col
              xs={24}
              sm={12}
              md={12}
              lg={12}
              className="item-description-container"
            >
              {<ItemShortDescription item={this.state.item} />}
            </Col>
          </Row>
          <Row>
            <ItemDetails item={this.state.item} />
          </Row>
        </React.Fragment>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return { isAdminLoggedIn: state.auth.isAdminLoggedIn };
};
const connectedItemPage = connect(mapStateToProps, null)(ItemPage);
export { connectedItemPage as ItemPage };
