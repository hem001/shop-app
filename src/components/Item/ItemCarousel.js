import React, { Component } from "react";

import { Carousel, Modal,  } from "antd";
import { showModal, closeModal } from "../../utils/componentUtils";
import "../../sass/item.sass";

export class ItemCarousel extends Component {
	constructor(props) {
		super(props);
		this.state = {
			show_modal: false,
			slide_no: 0,
		};
		this.clickCarousel = this.clickCarousel.bind(this);
		this.onChange = this.onChange.bind(this);
	}
	onChange(a, b, c) {
		this.setState({
			slide_no: a
		});
	}
	clickCarousel(e) {
		showModal.call(this);
		this.setState({
			slide_no: e.target.name
		});
	}

	render() {
		const CarouselImage = (props) => {
			return (
					<Carousel
						initialSlide={this.state.slide_no}
						afterChange={this.onChange}
						dots={true}
						speed={500}
						infinite={true}
						customPaging={i => (
							<div
								style={{
									width: "50px",
									height: "50px",
									color: "black",
									borderRadius: "5px",
									marginLeft: "5px",
									marginTop: "10px",
								}}
							>
								{
									<img
										src={this.props.images[i]}
										alt="cat not found"
										className= "image-thumbnail"
									/>
								}
							</div>
						)}
					>
						{this.props.images.map((i, j) => (
							<div 
	className={ `${(props.for_modal ==="true" ? "image-frame1" : "image-frame")}` }
							key={j}>
								<img src={i} alt="no data found found" name={j} onClick={this.clickCarousel}
								className="centered-image"
                />
							</div>
						))}
					</Carousel>
			);
		};


		return (
			<React.Fragment>
				<Modal
					visible={this.state.show_modal}
					onCancel={closeModal.bind(this)}
					onOk={closeModal.bind(this)}
					bodyStyle={{ width: "700px", height: "600px" }}
					width="800px"
					footer={null}
					style={{ textAlign: "center" }}
				>
					<div className="box-modal">
						<CarouselImage for_modal="true" />
					</div>
				</Modal>

				
				<div className="box-carousel">
					<CarouselImage for_modal="false" />
				</div>
			</React.Fragment>
		);
	}
}
