import React, { Component } from "react";
import { Link} from 'react-router-dom';
import { connect } from "react-redux";

import {ItemData} from '../../services';

// import {Table, Input, InputNumber, Popconfirm, Form} from 'antd'
import '../../sass/home.sass';


class InventoryMainPage extends Component {
  constructor(props){
    super(props);
    this.state={
      data_loaded: false,
      data: [],
      isAdminLoggedIn: false,
    }
  }
  componentDidMount(){
    ItemData.getCategoriesList()
    .then(res=>{
      
      this.setState({
        data_loaded: true,
        data: res.data.categories,
        isAdminLoggedIn: this.props.isAdminLoggedIn,

      });
    }
    ,err=>{
      console.log(err);
    })

  }
  render() {
    const heading = (this.state.isAdminLoggedIn) ? "Inventory Page" : "Categories";
    const url = (this.props.isAdminLoggedIn)? "/cms-dashboard/category/" : "/category/"; 

    if(this.state.data_loaded){
    return (
      <div className="home-body" >

      <div className="title">{heading}</div>
      <div className="items-display">
        {this.state.data.map((i)=>
          <div className="tile" style={{"height":"50px", "paddingTop":"10px", "paddingBottom":"10px"}} key={i}>
            <Link to={url + i} >
              <h2 align="center">{i} </h2>
            </Link>
          </div>
        )}
      </div>
    </div>
  )}
  else{
    return(
      <div className="">Nout</div>
    )
  }
}
}

const mapStateToProps = state =>{
  return {
    isAdminLoggedIn: state.auth.isAdminLoggedIn,
  };
};

const con = connect(mapStateToProps, null)(InventoryMainPage);

export {con as InventoryMainPage };
