import React, { Component } from 'react';
import { Table, Input, InputNumber, Popconfirm, Form,Button } from "antd";

import {ItemData} from '../../services';

import '../../sass/home.sass';
const EditableContext = React.createContext();

class EditableCell extends React.Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };

  renderCell = ({ getFieldDecorator }) => {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      children,
      ...restProps
    } = this.props;
    return (
      <td {...restProps}>
        {editing ? (
          <Form.Item style={{ margin: 0 }}>
            {getFieldDecorator(dataIndex, {
              rules: [
                {
                  required: true,
                  message: `Please Input ${title}!`,
                },
              ],
              initialValue: record[dataIndex],
            })(this.getInput())}
          </Form.Item>
        ) : (
          children
        )}
      </td>
    );
  };

  render() {
    return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
  }
}


export default class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data:this.props.data, item_id:this.props.item_id,editingKey: '' };
    this.columns = [
      {
        title: 'Key',
        dataIndex: 'key',
        width: '25%',
        
      },
      {
        title: 'Value',
        dataIndex: 'value',
        width: '45%',
        editable: true,
      },
      {
        title: 'Edit',
        dataIndex: 'operation',
        width: '30%',
        render: (text, record) => {
          const { editingKey } = this.state;
          const editable = this.isEditing(record);
          return editable ? (
            <span>
              <EditableContext.Consumer>
                {form => (
                  <Popconfirm
                  title="Do you want to save changes?"
                  onConfirm={() => this.save(form, record.key)}
                  >
                    <Button
                    size="small"
                      style={{ marginRight: 8 }}
                    >
                      Save
                    </Button>
                    
                  </Popconfirm>
                  
                )}
              </EditableContext.Consumer>
                <Button onClick={this.cancel} size="small" type="danger">Cancel</Button>
            </span>
          ) : (
            <Button type="primary"  disabled={editingKey !== ''} onClick={() => this.edit(record.key)}>
              Edit
            </Button>
          );
        },
      },
    ];
  }

  isEditing = record => record.key === this.state.editingKey;

  cancel = () => {
    this.setState({ editingKey: '' });
  };

  save(form, key) {
  	  	
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const newData = [...this.state.data];
      const index = newData.findIndex(item => key === item.key);
      if (index > -1) {
        const item = newData[index];
        
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        
        
        ItemData.editItemByKey({
        	item_id : this.state.item_id,
        	key: item.key,
        	value: row.value
        })
        .then(res=>{
          this.setState({ data: newData, editingKey: '' });
        	console.log('success');
        },err=>{
        	console.log('err',err);
          this.setState({ editingKey:''});
        })
      } else {
        newData.push(row);
        this.setState({ data: newData, editingKey: '' });
      }
    });
  }

  edit(key) {
  	
    this.setState({ editingKey: key });
  }

  render() {
    const components = {
      body: {
        cell: EditableCell,
      },
    };

    const columns = this.columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex === 'price' ? 'number' : 'text',
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });

    return (
      <EditableContext.Provider value={this.props.form}>
        <Table
          components={components}
          bordered
          dataSource={this.state.data}
          columns={columns}
          rowClassName="editable-row"
          pagination= {false}
          
        />
      </EditableContext.Provider>
    );
  }
}


