import React, { Component } from "react";

import { ItemData } from "../../services";
import { Row, Col, Form, Button, Popconfirm, Comment, Icon, Avatar, Rate, Divider } from "antd";

import EditableTable from "./EditableTable";
import { AddRemoveInputFields } from "../../utils/AddRemoveInputFields";
import { InputPair } from "../../utils/InputPair";
import { objectToList, listToObject } from "../../utils/componentUtils";

import "../../sass/home.sass";
class InventoryItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: {},
      data_loaded: false,
      message: "",
      images: [],
      ratings: [],
      other_details: [],
      item_id: this.props.match.params.item_id,
      comments: [],
    };

    this.onImageChange = this.onImageChange.bind(this);
    this.onRatingChange = this.onRatingChange.bind(this);
    this.onDataChange = this.onDataChange.bind(this);
    this.saveToDB = this.saveToDB.bind(this);
    this.deleteOneComment = this.deleteOneComment.bind(this);
  }
  deleteOneComment(comment){
    console.log('comment ',comment);
    var item_id = this.state.item_id;
    
    
    ItemData.deleteOneComment({
      item_id,
      comment,
    }).then(
    res=>{
      console.log('success', res);
      var tmp = this.state.comments;
      var i = tmp.indexOf(comment);
      tmp.splice(i,1);
      console.log('tmp ', tmp)
      this.setState({
        comments : tmp
      })
    },
    err =>{
      console.log(err);
    }
    );
  }
  saveToDB(key, value) {
    var item_id = this.state.item_id;

    ItemData.editItemByKey({
      item_id,
      key: key,
      value: value,
    }).then(
      (res) => {
        console.log("success");
      },
      (err) => {
        console.log("err", err);
      }
    );
  }
  onDataChange(data) {
    this.setState({
      other_details: data,
    });
  }

  onImageChange(data) {
    this.setState({
      images: data,
    });
  }
  onRatingChange(data) {
    this.setState({
      ratings: data,
    });
  }

  componentDidMount() {
    ItemData.getItemById(this.props.match.params.item_id).then(
      (res) => {
        this.setState({
          item: res.data.item,
          data_loaded: true,
        });
        if (res.data.item.images) {
          this.setState({
            images: res.data.item.images,
          });
        }
        if (res.data.item.other_details) {
          this.setState({
            other_details: objectToList(res.data.item.other_details),
          });
        }
        if (res.data.item.ratings) {
          this.setState({
            ratings: res.data.item.ratings,
          });
        }
        if (res.data.item.comments) {
          console.log("comments ", res.data.item.comments);
          this.setState({
            comments: res.data.item.comments,
          });
        }
      },
      (err) => {
        console.log(err);
        this.setState({
          message: "Data not found",
        });
      }
    );
  }
  render() {
    if (this.state.data_loaded) {
      // console.log("data ", this.state.item);
      const data = [];
      var images = [];
      for (const [k, v] of Object.entries(this.state.item)) {
        if (k !== "_id" && k !== "__v") {
          // console.log(k, "=>", v);
          if (typeof v === "object") {
            if (k === "images") {
              images = v;
            }
            // } else if (k === "item_ratings") {
            // } else if (k === "other_details") {

            // } else if (k === "comments") {
            //   // console.log("comments", v);
            // }
          } else {
            data.push({
              key: k,
              value: v,
            });
          }
        }
      }

      const EditableFormTable = Form.create()(EditableTable);

      return (
        <div className="home-body">
          <div className="title">Inventory Item</div>
          <div className="home-body">
            <EditableFormTable
              data={data}
              item_id={this.props.match.params.item_id}
            />
            <div className="home-body">
              {this.state.images ? (
                <Row>
                  <Col xs={8}>Images:</Col>
                  <Col xs={16}>
                    <AddRemoveInputFields
                      inputs={images}
                      imageChange={this.onImageChange}
                    />

                    <Popconfirm
                      title="Do you want to save the changes?"
                      name="images"
                      onConfirm={() => {
                        this.saveToDB("images", this.state.images);
                      }}
                      okText="Yes"
                      cancelText="No"
                    >
                      <Button
                      type='primary'
                      style={{float:'right'}}
                      >Save</Button>
                    </Popconfirm>
                  </Col>
                </Row>
              ) : null}

              {this.state.other_details ? (
                <Row>
                  <Col xs={8}>Other Details:</Col>
                  <Col xs={16}>
                    <InputPair
                      data={this.state.other_details}
                      onDataChange={this.onDataChange}
                    />
                    <Popconfirm
                      title="Do you want to save the changes?"
                      name="images"
                      onConfirm={() => {
                        this.saveToDB(
                          "other_details",
                          listToObject(this.state.other_details)
                        );
                      }}
                      okText="Yes"
                      cancelText="No"
                    >
                      <Button
                      type='primary'
                      style={{float:'right', background:''}}
                      >Save</Button>
                    </Popconfirm>
                  </Col>
                </Row>
              ) : null}
              {(this.state.comments.length) ?(
                <div className="">
                <Row>
                 Comments:
                </Row>
                <Row>
                  <Col xs={2} >
                  </Col>
                  <Col xs={22} >
                  {this.state.comments.map((i,j)=>(
                    
                    <div className="" key={j}>
                    {j.submitted_by}
                   <Comment
                   author={i.submitted_by}
                   avatar={
                    <Avatar
                    src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"
                    
                    />
                   }
                   content={
                    <div className="">
                    <Rate value={i.stars} disabled></Rate>
                    <p>{i.comment}</p>
                    </div>}
                   
                   >
                     
                   </Comment>
                   <Popconfirm
                   title="Do you want to delete the comment"
                   onConfirm={()=>this.deleteOneComment(i)}>
                  <Button 
                  type="danger"
                  
                  >Delete</Button>
                   </Popconfirm>
                    
                    <Divider />
                    
                    
                    </div>)
                  )}
                    
                    
                    
                  </Col>
                </Row>
                </div>
                ):(null)}
            </div>

            <Button
              type="primary"
              block
              onClick={() => {this.props.history.push('/cms-dashboard/inventory');
              }}
            >
              Done
            </Button>
          </div>
        </div>
      );
    } else if (!this.state.data_loaded) {
      return (
        <div className="home-body">
          <div className="title">
            {!this.state.message
              ? "Loading to load Data..."
              : this.state.message}
          </div>
        </div>
      );
    }
  }
}

export { InventoryItem };
