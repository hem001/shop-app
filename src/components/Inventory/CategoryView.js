import React, { Component } from "react";
import { Row, Col, Divider,Button,  } from "antd";
import {Link} from 'react-router-dom'
import { connect } from "react-redux";

import { HomePageItem } from "../Home/HomePageItem";
import { ItemData } from "../../services";
import "../../sass/home.sass";

class CategoryView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data_loaded: false,
      data: [],
      view: "grid",
      params: "",
      message: "",
    };
  }
  componentDidMount() {
    ItemData.getItemsByCategory(this.props.match.params.category).then(
      (res) => {
        this.setState({
          data_loaded: true,
          data: res.data.items,
          params: this.props.match.params.category,
          message:"",
          
        });
        if(!res.data.items.length){
          this.setState({
          message: "No data found in this category",
        })
        }

      },
      (err) => {
        console.log(err);
        this.setState({
          message: "Error loading the data",
        })
      }
    );
  }
  render() {
    const url = (this.props.isAdminLoggedIn) ? "/cms-dashboard/item/" : "/item/"
    if (this.state.data_loaded) {

      if (this.props.match.params.category !== this.state.params) {
        this.componentDidMount();
      }

      return (
        <div className="home-body">
          <div className="title">Category: {this.state.params} </div>
          <div className="home-body">
            <div style={{ textAlign: "right" }}>
              View: 
              <Button onClick={()=>this.setState({view:'list'})} >
                List
              </Button>
              <Divider type="vertical" />
              <Button onClick={()=>this.setState({view:'grid'}) }>
                Grid
              </Button>
            </div>
            <div className="title">{this.state.message}</div>

            <div className={(this.state.view==='list')?"" :"items-display"}>
              {this.state.data.map((i) => (
                <div key={i._id}>
                <Link to={url + i._id } >
                  
                
                  {this.state.view === "list" ? (
                    <ListView item={i} />
                  ) : (
                    <div className="tile">
                      <HomePageItem item={i} />
                    </div>
                  )}
                  </Link>
                </div>
              ))}
            </div>
          </div>
        </div>
      );
    } else {
      return <div className=""><h2 align="center">{this.state.message}</h2></div>;
    }
  }
}
function ListView({ item }) {
  return (
    <div className="tile" style={{ width: "100%", padding: "10px" }}>
      <Row>
        <Col xs={5}>
          <img src={item.images[0]} height="50px" alt="..." />
        </Col>
        <Col xs={19}>
          Name: {item.name}
          <Divider type="vertical" />
          Price: £{item.price}
          <Divider type="vertical" />
          Brand: {item.brand}
          <Divider type="vertical" />
        </Col>
      </Row>
    </div>
  );
}

const mapStateToProps =state=>{
  return {
 isAdminLoggedIn: state.auth.isAdminLoggedIn
}
};

const con = connect(mapStateToProps, null)(CategoryView);
export { con as CategoryView };
