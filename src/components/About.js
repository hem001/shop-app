import React, { Component } from "react";
import { Card } from "antd";
import "../sass/about.sass";

class About extends Component {
	render() {
		return (
			<div>
				<Card className="about-card-style">
					<div className="card-header">
						<h2 align="center">About</h2>
					</div>

					<div className="about-card-body">
						<p>
							Consectetur in laborum et deserunt dolor velit
							cupidatat mollit nulla proident proident officia do
							ut do incididunt ad consequat magna velit ullamco
							sint cillum ut ut anim duis pariatur laborum fugiat
							in consequat ullamco occaecat in commodo laborum.
						</p>
						<p>
							Commodo dolore dolore esse veniam cupidatat duis in aliqua et duis anim laboris et cupidatat pariatur occaecat ea occaecat deserunt dolor quis anim tempor veniam est ut anim aliqua quis amet tempor mollit sed anim dolore id dolor ut laborum consequat enim commodo aute cillum laborum ea id consectetur culpa eiusmod reprehenderit elit mollit sint mollit nulla deserunt aliqua ad ut est velit et laborum aliqua sunt irure elit consequat labore irure ut sunt cupidatat reprehenderit ea aliquip quis ex culpa proident ad quis labore dolore in officia aliqua amet sunt fugiat enim occaecat deserunt dolore tempor velit in ut incididunt in dolor magna occaecat reprehenderit eu qui pariatur dolor ut reprehenderit eu exercitation sit ea dolore quis eu excepteur ea adipisicing occaecat exercitation velit in nulla aliquip consequat magna magna exercitation sit anim excepteur nisi proident et elit amet non exercitation voluptate incididunt ullamco eu in ex anim ut in amet velit sint exercitation ea anim non dolor tempor excepteur enim ullamco voluptate eu in.
						</p>
					</div>
				</Card>
			</div>
		);
	}
}
export default About;
