import React, { Component } from "react";
import { connect } from "react-redux";


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import "antd/dist/antd.css";
import { Auth } from "../services";

//CMS related routes
import { CMSLogin } from "./CMS/CMSLogin";
import { RegisterAdmin } from "./CMS/RegisterAdmin";
import { CMSDashboard } from "./CMS/Dashboard/CMSDashboard";

import {HomePage} from "./Home/HomePage";
import {Login } from "./LoginPage";
import About from "./About";
import Contact from "./Contact";
import Register from "./Register";
import { ItemPage } from "./Item/ItemPage";
import { ShoppingCart } from "./Payment/ShoppingCart";
import { NavigationBar } from "./NavigationBar";

import { InventoryMainPage } from "./Inventory/InventoryMainPage";
import { CategoryView } from "./Inventory/CategoryView";


class RouterComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAdminLoggedIn: Auth.isAdminLoggedIn(),
    };
  }

  render() {
    const Navi = () => {
      if (this.props.isAdminLoggedIn) {
        return null;
      } else {
        return <NavigationBar />;
      }
    };

    return (
      <React.Fragment>
        <Router>
          {/*<p>here: {this.props.isAdminLoggedIn} </p>*/}
          <Navi />

          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/About">
              <About />
            </Route>
            <Route path="/Contact">
              <Contact />
            </Route>
            <Route path="/register">
              <Register />
            </Route>
            <Route path="/item/:item_id" component={ItemPage} />
            <Route path="/shopping-cart" component={ShoppingCart} />
            
            <Route path="/categories" component={InventoryMainPage} />
            <Route path="/category/:category" component={CategoryView} />

            <Route path="/cms">
              {Auth.isAdminLoggedIn() ? (
                <Redirect to="/cms-dashboard" />
              ) : (
                <CMSLogin />
              )}
            </Route>
            }
            <Route path="/cms-register" component={RegisterAdmin} />
            <PrivateRoute
              path="/cms-dashboard"
              component={CMSDashboard}
            ></PrivateRoute>
            
            <Route path="/*" render={()=>{return <div className="title">Page not found!!</div>}} />
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}
const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        Auth.isAdminLoggedIn() ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/" }} />
        )
      }
    />
  );
};

const mapStateToProps = (state) => {
  return {
    isAdminLoggedIn: state.auth.isAdminLoggedIn,
    username: state.auth.username,
  };
};

const connectedRouter = connect(mapStateToProps, null)(RouterComponent);

export { connectedRouter as RouterComponent };
