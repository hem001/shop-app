import React, { Component } from "react";
import {connect} from 'react-redux';
import { Link } from "react-router-dom";

import '../../sass/home.sass';

import { HomePageCarousel } from './HomePageCarousel';
import { HomePageItem } from './HomePageItem';
import {Row, Col} from "antd";
import {ItemData} from '../../services';

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      data_loaded: false,
    };
  }
  componentDidMount(){
    ItemData.getHomePageData()
    .then(response =>{
      this.setState({items: response.data.items,
        data_loaded: true,}
        );
      }
      ,error=>{
        console.log('Error ',error);
      })

  }
  render() {
    const url = '/item/';
    if(this.state.data_loaded){
    return (
      <div className="App">
      <h1 align="center">Home</h1>
      <div className="home-body">
        <HomePageCarousel></HomePageCarousel>
        <Row>
          <Col xs={24} sm={24} md={18}>
            <div className="items-display">
            {this.state.items.map((i)=>
              <div className="tile" key={i._id}>
              <Link to= {url + i._id}>
                <HomePageItem item = {i} /> 
              </Link>
                
              </div>
              )}
            </div>
          </Col>
          <Col xs={24} sm={24} md={6}>
          <p> Lorem ipsum qui fugiat laborum officia commodo.</p>
  
          </Col>
        </Row>
      </div>

    </div>
    );}
    else{
      return(
        <div className="">Data not loaded</div>)
    }
  }
}
// const mapStateToProps = state=>{
//   return {itemList: state.itemList};
// };
const connectedHomePage = connect(null, null)(HomePage);

export {connectedHomePage as HomePage};
