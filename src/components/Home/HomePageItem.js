import React, { Component } from "react";
import {Card, } from "antd";
const {Meta} = Card;


class HomePageItem extends Component {
	render() {
		return (
			<Card hoverable
				style={{ textAlign:"center" }}
				cover={
					<img
						alt="example"
						src= {this.props.item.images[0]}
					/>
				}>

				<Meta
					title={"Name: "+this.props.item.name}
				/>
        Price: £ {this.props.item.price}<br/>
        Brand: {this.props.item.brand}
			</Card>

		);
	}
}

export { HomePageItem };
