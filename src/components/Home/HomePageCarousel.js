import React, { Component } from "react";
import {Carousel} from 'antd';

class HomePageCarousel extends Component {
	render() {
		return (
			<>
				<div className="carousel-top">
					<Carousel 
					autoplay
					>
						<div className="">
						  <img src="https://visualmodo.com/wp-content/uploads/2018/03/Graphic-Design-Learning-Tips.jpg" height="200px" width="100%"  alt=""/>
						</div>
						<div>
						  <img src="https://public-media.interaction-design.org/images/ux-daily/5628f8c6cdb9d.jpg" height="200px" width="100%"  alt=""/>
						</div>
						<div>
						  <img src="https://storage.googleapis.com/gd-wagtail-prod-assets/original_images/design_is_never_done_share.png" height="200px" width="100%"  alt=""/>
						</div>
						<div>
						  <img src="https://cdn.imgbin.com/22/21/16/imgbin-pixel-art-graphic-design-cool-designs-pic-music-equalizer-illustration-N1ELk1B6hP6SSxjxDpmeSyFc8.jpg" height="200px" width="100%"  alt=""/>
						</div>
					</Carousel>
				</div>
			</>
		);
	}
}

export { HomePageCarousel };
