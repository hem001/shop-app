import React, { Component } from "react";
import { connect } from "react-redux";

import { Icon, Card, Row, Col, InputNumber, Button,Modal,message,Popconfirm } from "antd";

import {PaypalButton} from 'react-paypal-button-v2';

import { cartActions } from "../../redux/actions/index";
import {ItemData} from "../../services";
import {OrderData} from '../../services'
import "../../sass/shopping_cart.sass";

/*
shoppint_cart = [{_id: '', quantity:''}]
*/

class ShoppingCart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      fetched_data: false,
      data_found: false,
      shopping_cart_empty: false,
      total: 0,
      show_paypal: true,
    };


    this.removeItemFromCart = this.removeItemFromCart.bind(this);
    this.updateShoppingCart = this.updateShoppingCart.bind(this);
    this.clearShoppingCart = this.clearShoppingCart.bind(this);
    this.proceedPayment = this.proceedPayment.bind(this);
  }
  componentDidMount() {

    if (this.props.shopping_cart) {

      var data = [];
      var total = 0;
      for (let i = 0; i < this.props.shopping_cart.length; i++) {
        // var quantity = this.props.shopping_cart[i].quantity;
        ItemData.getItemById(this.props.shopping_cart[i]._id).then(
          (res)=>{
            let item = res.data.item;
            item['quantity'] = this.props.shopping_cart[i].quantity;
            data.push(res.data.item);
            // data =[...data,  ]
            total += (res.data.item.price * this.props.shopping_cart[i].quantity);
            this.setState({
              data,
              fetched_data: true,
              data_found: true,
              total,
            });
          },
          error => {
            console.log("Error fetching data");
            this.setState({
              data_found: false,
            });
          }
        );
      }
    } else {
      this.setState({
        shopping_cart_empty: true,
      });
    }
  }
  
  proceedPayment(){
    //update cart list in store before proceeding.
    //if total amount to pay is Zero
    if(this.state.total===0){
      //clear cart list in both store and local space
      this.setState({
        data:[],
        fetched_data:false,
        data_found: false,
        shopping_cart_empty: false,
        total:0,
      });
      this.props.dispatch(cartActions.clearShoppingCart());
      message.info('Nothing to pay');
      this.props.history.push('/');
      return;
    }

    //if there is amount to pay
    //update cart{_id and quantity}

    //check if user is logged in
    if(!this.props.isUserLoggedIn){
      console.log('not logged in');
      message.info("Please login to proceed to payment");

      this.props.history.push('/login');
      return;
    }

   
      var temp_cart = [];
      for(let i = 0; i< this.state.data.length;i++){
        temp_cart.push({
          _id: this.state.data[i]._id,
          quantity: this.state.data[i].quantity,
        })
      }
      
    this.props.dispatch(cartActions.updateShoppingCart(temp_cart));
    // console.log('store cart', this.props.shopping_cart);

    //add to order cart and total
    
    /*turn <PaypalButton
    amount = "100"
    onSuccess={()=>{
      message.info('Transaction completed..');
    }
    }
    options={{
      clientId: "AUcbiAS8eWz9oBYTz8-dZK2oMKYkx-umIeuRLkONArCnFxr_tI_7yw5oyhBLxEAfaN6AfoHNuPTFhme_"
    }}
    />*/
    const data = {
      shopping_cart: this.props.shopping_cart,
      username: this.props.username,
      user_id: this.props.user_id,
      order_date: new Date(),
      total: this.state.total,
      order_completed: 'NO',
    }
    OrderData.addOrder(data)
    .then(response=>{
      console.log('order sent to api',data);
      this.props.dispatch(cartActions.clearShoppingCart());
      message.success("Thank you for the payment");
    }
    ,error=>{
      console.log("error sending order to api");
      message.error("Sorry, something went wrong");
    })

  }

  removeItemFromCart(index){
    console.log("here", index);
    let temp_data = [...this.state.data];
    let tot = this.state.total - ( temp_data[index].price *temp_data[index].quantity);
    console.log('toto ',temp_data[index]._id);
    this.props.dispatch(cartActions.removeFromShoppingCart(temp_data[index]._id,));
    temp_data.splice(index,1);

    this.setState({
      data: temp_data,
      total: tot,
    });


  }


  updateShoppingCart( item, index,val){
    //update total and the data[] list
    let temp_tot = this.state.total - (item.price* (item.quantity-val));
    let temp_data = [...this.state.data];
    temp_data[index].quantity = val;
    //if value is 0
    if(val === 0){

    }
    //update shopping cart in store
    this.props.dispatch(cartActions.updateShoppingCart({
      _id: temp_data[index]._id,
      quantity: temp_data[index].quantity,
    }));
    
    //update state
    this.setState({
      total: temp_tot,
      data: temp_data,
    });
    console.log("shopping cart",this.props.shopping_cart);


  }

  clearShoppingCart() {
    this.props.dispatch(cartActions.clearShoppingCart());
    this.setState({
      data: [],
      total: 0,
    });
    console.log(this.props.shopping_cart);
  }

  render() {
    if (this.state.fetched_data && this.state.data_found) {
      return (
        <div className="cart-content" >
          <Row>
            <Col md={16}>
              <Card className="cart-card-style">
                <div className="cart-header">
                  <h2>
                    <Icon type="shopping-cart" /> Shopping Cart
                  </h2>
                </div>
                <div className="cart-card-body">
                  {this.state.data.map((i, j) => (
                    <div className="" key={j}>
                      <CartItem 
                        item={i} 
                        index ={j}
                        updateShoppingCart= {(val)=>this.updateShoppingCart(i,j, val)}
                        removeItemFromCart = {()=>this.removeItemFromCart(j)}
                        />
                    </div>
                  ))}
                </div>
              </Card>
            </Col>

            <Col md={8}>
              <Card className="cart-card-style">
                <div className="cart-header">
                  <h3>Total</h3>
                </div>
                <div className="cart-card-body">
                  <Pay 
                      item={this.state.data} 
                      total={this.state.total}
                      clearShoppingCart = {this.clearShoppingCart}
                      proceedPayment = {this.proceedPayment} />
                </div>
              </Card>
            </Col>
          </Row>
          {/*<div className="">
            {this.state.show_paypal && <PaypalButton
          amount = "100"
          onSuccess={()=>{
            message.info('Transaction completed..');
          }
          }
          options={{
            clientId: "AUcbiAS8eWz9oBYTz8-dZK2oMKYkx-umIeuRLkONArCnFxr_tI_7yw5oyhBLxEAfaN6AfoHNuPTFhme_"
          }}
          />}
          </div>*/}
        </div>
      );

    } else if (this.state.shopping_cart_empty) {
      return <h1 align="center"> Empty shopping cart</h1>
    } else if (!this.state.data_found) {
      return <h1 align="center"> No data found</h1>
    } else {
      return <p>Fetching Data</p>;
    }
  }
}

function CartItem({ item , updateShoppingCart, removeItemFromCart, }) {
  const {confirm} = Modal;
  function showDeleteConfirm() {
    confirm({
      title: "Do you want to delete the item?",
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk(){
        removeItemFromCart();
      },
    })
  }
  function checkZeroVal(val) {
    console.log('check val',val)  
    if(val === 0){
      showDeleteConfirm();
    }
    updateShoppingCart(val);
  }
  return (
    <Row className="space-out cart-item-border">
      <Col sm={8}>
        <img width="100px" src={item.images[0]} alt="cat goes here" />
      </Col>
      <Col sm={14}>
      <h4>
        <Col sm={12}> Name: </Col><Col sm={12}>{item.name}</Col>
        <Col sm={12}> Price: </Col><Col sm={12}>{item.price}</Col>

        <Col sm={12}> 
          <span>
            Quantity:
            <InputNumber defaultValue={item.quantity} min={0}
              onChange={checkZeroVal}
            />
          </span>
        </Col > 
        <Col sm={12}> 
         <Button type="link" 
         onClick={showDeleteConfirm}
         >
          Remove</Button>
         </Col> 
         </h4>
      </Col>

    
    
    </Row>
  );
}

function Pay({ item, total, clearShoppingCart, proceedPayment }) {

  return (
    <div className="">
      <Row>
        <h3>
          <Col span={12}>Total</Col>
          <Col span={12} > £{total} </Col>
        </h3>
      </Row>

      <Row className="space-out">
      <Popconfirm title="Do you wish to proceed?"
      onConfirm={proceedPayment}
      >
      <Button type="primary"
      block>
      Pay
      </Button>
      </Popconfirm>
        
      </Row>

      <Row>
        <Button className ="space-out"
        block
        onClick = {clearShoppingCart}
        >
        Clear Shopping Cart
        </Button>

      </Row>
    </div>
  );
}

const mapStateToProps = state => {
  return {
    shopping_cart: state.cart.shopping_cart,
    isUserLoggedIn: state.auth.isUserLoggedIn,
    username: state.auth.username,
    user_id: state.auth.user_id,
  };
};
var con = connect(mapStateToProps, null)(ShoppingCart);
export { con as ShoppingCart };