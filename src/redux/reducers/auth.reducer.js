import { actionTypes } from "../constants";
const initialState ={
  isUserLoggedIn: false,
  isAdminLoggedIn: false,
  username: "",
  user_id: "",
}

export function auth(state = initialState, action) {

  switch (action.type) {
    case actionTypes.LOGIN_REQUEST:
    console.log('reducer ',action.data);
      return Object.assign({}, state, {
        username: action.data.username,
        user_id : action.data._id,
        isUserLoggedIn: true
      });

    case actionTypes.LOGOUT:
      return Object.assign({}, state, { isUserLoggedIn: false,username:'', user_id:'' });

    case actionTypes.ADMIN_LOGIN_REQUEST:
      return Object.assign({}, state, {
        username: action.data.username,
        user_id: action.data._id,
        isAdminLoggedIn: true
      });

    case actionTypes.ADMIN_LOGOUT:
      return Object.assign({}, state, initialState);

    default:
      return state;
  }
}
