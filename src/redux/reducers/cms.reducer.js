import {actionTypes} from '../constants';
const INITIAL_STATE={
  cms_item_data: [],
}

export function cms(state=INITIAL_STATE, action) {
  switch (action.type) {
    // case actionTypes.STOREDATA:
    //   return Object.assign({}, state, {itemList: action.itemList});
      
      //save item details when adding new item
      case actionTypes.CMS_SAVE_ITEM:
      console.log('here test it');
      return Object.assign({}, state, {
        cms_item_data: [...state.cms_item_data, action.data] ,
      })  ;

      //get item details when posting new item to server
      case actionTypes.CMS_GET_ITEM:
      console.log('there test it2222');
      return state;

      //cms clear cms_item_data containing item details
      case actionTypes.CMS_CLEAR_ITEM:
      return Object.assign({}, state,{
        cms_item_data: [],
      });

      case actionTypes.RESET_ACTION:
      return INITIAL_STATE;

      default:
        return state;
    }

}