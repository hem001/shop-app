import {combineReducers} from 'redux';

import {auth} from './auth.reducer';
import {cms} from './cms.reducer';
import {cart} from './cart.reducer';

export const rootReducer = combineReducers({
  auth, 
  cms,
  cart,
});



