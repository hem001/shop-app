import {actionTypes} from '../constants';
const initialState={
    shopping_cart: [],
}

export function cart(state = initialState, action) {
  switch (action.type) {
    //shopping-cart
    //clear shopping-cart
    case actionTypes.CLEAR_SHOPPING_CART:
      return Object.assign({}, state, {
        shopping_cart: [],
      });
      
      //add to shopping cart
    case actionTypes.ADD_TO_SHOPPING_CART:
      var temp = [...state.shopping_cart];
      for(let i =0;i< temp.length;i++){
        //check if item already in the cart, then add to prev count
        if(temp[i]._id === action.data._id){
          temp[i].quantity += action.data.quantity;
          return Object.assign({}, state,{
            shopping_cart: temp,
          })
        }
      }
      return Object.assign({}, state, {
        shopping_cart: [...state.shopping_cart, action.data],
      });
      //remove item from shopping cart
    case actionTypes.REMOVE_FROM_SHOPPING_CART:
      temp = [...state.shopping_cart];
      temp = temp.filter((d) => { return d._id !== action.data; });
      return Object.assign({}, state, {
        shopping_cart: temp,
      });

    case actionTypes.UPDATE_SHOPPING_CART:
    temp = [...state.shopping_cart];
    for(let i = 0; i<temp.length; i++){
        if(temp[i]._id === action.data._id){
          temp[i].quantity = action.data.quantity;
        }
    }

    return Object.assign({}, state, {
      shopping_cart: temp,
    });



    default:
      return state;
  }
}