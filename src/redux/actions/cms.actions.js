import {actionTypes} from '../constants';


export const cmsActions ={
  cms_saveItem,
  cms_getItem,
  cms_clearItem,
  resetState,

  
}
function cms_saveItem(data) {
  return{
    data: data,
    type: actionTypes.CMS_SAVE_ITEM,
  }
}
function cms_getItem() {
  return{
    type: actionTypes.CMS_GET_ITEM,
  }
}
function cms_clearItem() {
  return{
    type: actionTypes.CMS_CLEAR_ITEM,
  }
}
function resetState() {
  return{
    type: actionTypes.RESET_ACTION,
  }
};

