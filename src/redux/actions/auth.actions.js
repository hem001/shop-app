import {actionTypes} from '../constants';

export const authActions ={
  login, 
  logout,
  loginAdmin,
  logoutAdmin,
};



function logoutAdmin() {
  return{
    type: actionTypes.ADMIN_LOGOUT
  };
}
function loginAdmin(data) {
  return{
    data:data,
    type: actionTypes.ADMIN_LOGIN_REQUEST,
  };
}

function login(data){
  return {
    data: data,
    type: actionTypes.LOGIN_REQUEST,
  };
}
function logout(){
  return {
    type: actionTypes.LOGOUT,
  };
}


