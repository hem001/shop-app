import {actionTypes} from '../constants';

export const cartActions ={
    clearShoppingCart,
    addToShoppingCart,
    removeFromShoppingCart,
    updateShoppingCart,

};
function updateShoppingCart(data) {
  return{
    type: actionTypes.UPDATE_SHOPPING_CART,
    data: data,
  }
}
function clearShoppingCart() {
  return{
    type: actionTypes.CLEAR_SHOPPING_CART,
  };
}
function removeFromShoppingCart(item_id) {
  return{
    data: item_id,
    type: actionTypes.REMOVE_FROM_SHOPPING_CART,
  };
}
function addToShoppingCart(data) {
  return{
    data: data,
    type: actionTypes.ADD_TO_SHOPPING_CART,
  };
}