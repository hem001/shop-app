require('dotenv').config();

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const uglifyJs = require("uglify-js");
const fs = require('fs');

const passport = require('passport');
const helmet = require('helmet');

require('./app_api/models/api.db_connection');
require('./app_api/config/api.passport');

const cors = require('cors');


const routerApi = require('./app_api//api.routes');

var app = express();

//middleware need to enable CORS functionality
app.use(cors({
    origin:'http://localhost:3000',
    optionsSuccessStatus: 200
}));
//cors middleware needed for enabling CORS(Cross Origin Requests)
// app.use(cors());
/*//use helmet 
app.use(helmet.hidePoweredBy({setTo: 'PHP 4.2.0'}));
//to stop others from adding our page as iframes
app.use(helmet.frameguard({action: 'deny'}));
//to protect from Cross-site-Scripting
app.use(helmet.xssFilter());
//protect from MIME sniffing
app.use(helmet.noSniff());
//protect from untrusted HTML download
app.use(helmet.ieNoOpen());


//protect from protocol downgrade attacks and cookie hijacking
var ninetyDaysInSeconds = 90*24*60*60;
app.use(helmet.hsts({maxAge: ninetyDaysInSeconds, force:true}));
//stop DNS prefetching only use if you need high security- slows down performance
// app.use(helmet.dnsPrefetchControl());

app.use(helmet.contentSecurityPolicy({
  directives:{
    defaultSrc: ["'self'"],
    scriptSrc: ["'self'"]
  }
}));

//to include noCache() and contentSecurityPolicy()
app.use(helmet({
  frameguard: {              // configure
    action: 'deny'
  },
  contentSecurityPolicy: {   // enable and configure
   directives: {
     defaultSrc: ["'self'"],
     scriptSrc: ["'self"],
   }
  },
 dnsPrefetchControl: false   // disable
}));

*/

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
//used to point resource to app_client folder
app.use(express.static(path.join(__dirname, 'app_client')));
//needed for using /node_modules path
app.use(express.static(path.join(__dirname)));


app.use('/api', routerApi);
app.use(passport.initialize());


//route to app_client index.html
app.use(function(req, res) {
	res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
	var err = new Error('Not found');
	err.status = 404;
	next(err);
});

//Catch unauthorised errors
app.use(function(err, req, res, next) {
	if (err.name === 'UnauthorizedError') {
		res.status(401);
		res.json({ "message": err.name + ": " + err.message });
	}
});



// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.render('error');
});
app.listen(process.env.PORT || 8000, () => {
  console.log("Listening on port " + process.env.PORT);
});


module.exports = app;