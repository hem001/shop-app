var mongoose = require("mongoose");
var crypto = require("crypto");
var jwt = require("jsonwebtoken");


var orderListSchema = new mongoose.Schema({
  order_id: {
    type: String,
  },
  username: {
    type:String,
    required: true,
  },
  user_id: {
    type: String,
    required: true,
  },
  shopping_cart: {
    type: [{}],
    required: true,
  },
  order_date: {
    type: String,
    required: true,
  },
  total:{
    type: Number,
    required: true,
  },
  order_completed:{
    type: String,
    default: 'NO',
    required:true,
  }

});

var commentSchema = new mongoose.Schema({
  submitted_by: {
    type: String,

  },
  comment: {
    type: String,
 
  },
  stars: {
    type: Number
  },
  _id:false,
});

var productSchema = new mongoose.Schema({
  _id: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  quantityInInventory: {
    type: Number
    // required: true
  },
  quantityToAdd: {
    type: Number
    // required:true
  },
  category: {
    type: String,
    required: true
  },

  description: {
    type: String,
    required: true
  },
  images: {
    type: [String],
    required: true
  },
  brand: {
    type: String,
    required: true
  },
  dimension: {
    type: String,
    required: true
  },
  weight: {
    type: String,
    required: true
  },
  color: {
    type: String,
    required: true
  },

  other_details: {},
  comments: [commentSchema],
  item_ratings:{
    type: [Number],
    default: [0,0,0,0,0]
  }
});

var userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  hash: String,
  salt: String
  // image_source: String
});

userSchema.methods.setPassword = function(password) {
  this.salt = crypto.randomBytes(16).toString("hex");
  this.hash = crypto
    .pbkdf2Sync(password, this.salt, 1000, 64, "sha512")
    .toString("hex");
  // console.log('enter ',this.salt,'            ',this.hash)
};

userSchema.methods.validatePassword = function(password) {
  console.log("validating password");
  var hash = crypto
    .pbkdf2Sync(password, this.salt, 1000, 64, "sha512")
    .toString("hex");
  return this.hash === hash;
};

userSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);

  return jwt.sign(
    {
      _id: this._id,
      email: this.email,
      name: this.name,
      exp: parseInt(expiry.getTime() / 1000)
    },
    process.env.JWT_SECRET
  );
};

mongoose.model("User", userSchema, "user");
mongoose.model("Items", productSchema, "items");
mongoose.model("AdminUser", userSchema, "admin_user");
mongoose.model("OrderList", orderListSchema, "order_list");
