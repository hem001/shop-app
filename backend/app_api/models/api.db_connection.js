var mongoose = require('mongoose');
var gracefulShutdown;
// var dbURI = 'mongodb://localhost:27017/findmoney';
var dbURI;

if (process.env.NODE_ENV === 'production'){
    dbURI = process.env.MONGOLAB_URI;

} else{
    dbURI = 'mongodb://localhost:27017/f';
}


//use options to remove deprecation warning messages
mongoose.connect(dbURI, {
  useUnifiedTopology: true,
  useNewUrlParser:true,
  useFindAndModify:false});
mongoose.connection.on('connected', () => {
    console.log('mongoose connected to the Mongoose Database');
});
mongoose.connection.on('error', (error) => {
    console.log(' Error ' + error);
});
mongoose.connection.on('disconnected', () => {
    console.log('mongoose disconnected  ');
});
gracefulShutdown = (msg, callback) => {
    mongoose.connection.close(() => {
        console.log('Mongoose disconnected through ' + msg);
        callback();
    });
};
//For nodemon restarts
process.once('SIGUSR2', () => {
    gracefulShutdown('nodemon restart', () => {
        process.kill(process.pid, 'SIGUSR2');
    });
});
//For app terminstaion
process.on('SIGINT', () => {
    gracefulShutdown('app termination', () => {
        process.exit(0);
    });
});
require('./api.db_schemas');