var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');
var AdminUser = mongoose.model('AdminUser');


passport.use(new LocalStrategy({
    usernameField: 'email',
    passReqToCallback: 'true',

}, 
function(req,username, password, done){
    var DB= User;
    if(req.body.admin_user){
        DB=AdminUser;
    }
    DB.findOne({email:username}, function(err, user){
        
        if(err){return done(err);}
        if(!user){
            return done(null, false, {
                message: 'Incorrect username.'
            });
        }
        if (!user.validatePassword(password)) {
            return done(null, false,{
                message: 'Incorrect password.'
            });
        }
        return done(null, user);
    });
}
));