var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
    secret: process.env.JWT_SECRET,
    userProperty: 'payload'
});


var ctrlAuth = require('./controllers/api.authentication');
var ctrlUsers = require('./controllers/api.users');
var ctrlInventory = require('./controllers/api.inventory');
var ctrlOrders = require('./controllers/api.orders');
var ctrlCategory = require('./controllers/api.categories');


router.get('/getUsers', ctrlUsers.getUsers);


//user authentication
router.post('/register',ctrlAuth.register);
router.post('/login', ctrlAuth.login);
//for registering admin user
router.post('/register-admin', ctrlAuth.registerAdminTrap);
//disable once admin has been registered in the system
// router.post('/register-admin', ctrlAuth.registerAdmin);



//Inventory management
router.post('/addItem', ctrlInventory.addItem);
router.get('/getAllItems', ctrlInventory.getAllItems);
router.get('/getItemById/:itemId', ctrlInventory.getItemById);
router.delete('/deleteItem/:itemId', ctrlInventory.deleteItemById);
router.put('/editItem/:itemId', ctrlInventory.editItemById);
router.put('/editItemByKey', ctrlInventory.editItemByKey);
router.put('/addReviewById/:itemId', ctrlInventory.addReviewById);

router.put('/deleteOneComment', ctrlInventory.deleteOneComment);


//Order management
router.get('/getOrderList', ctrlOrders.getOrderList);
router.get('/getOrderById/:order_id', ctrlOrders.getOrderById);
router.post('/addOrder', ctrlOrders.addOrderToList);
// router.delete('/deleteOrderById', ctrlOrders.deleteOrderById);
router.get('/getPendingOrdersList', ctrlOrders.getPendingOrdersList);
router.get('/getCompletedOrdersList', ctrlOrders.getCompletedOrdersList);
router.put('/orderCompleted', ctrlOrders.orderCompleted);
router.put('/undoOrderCompleted', ctrlOrders.undoOrderCompleted);

// category management
router.get('/getCategoriesList', ctrlCategory.getCategoriesList );
router.get('/getItemsByCategory/:category', ctrlCategory.getItemsByCategory);


module.exports = router;
