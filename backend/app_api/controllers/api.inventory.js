var mongoose = require("mongoose");
var ObjectId = require("mongodb").ObjectId;
var User = mongoose.model("User");
var Items = mongoose.model("Items");

var sendJsonResponse = function (res, status, content) {
  res.status(status);
  res.json(content);
};

//add review by id
module.exports.addReviewById = async function (req, res) {
  console.log("add review by id", req.body);
  var comment = {
    submitted_by: req.body.submitted_by,
    comment: req.body.comment,
    stars: req.body.stars,
  };
  if (req.body._id) {
    const doc = await Items.findById(req.body._id);
    doc.comments.push(comment);
    await doc.save(function (err, doc) {
      if (err) {
        sendJsonResponse(res, 404, err);
      } else {
        //update item_ratings
        doc.item_ratings = req.body.item_ratings;

        doc.save(function (err, item) {
          if (err) {
            sendJsonResponse(res, 404, err);
          } else {
            sendJsonResponse(res, 200);
          }
        });
      }
    });
  }
};

//edit item by key
module.exports.editItemByKey = async function (req, res) {
  console.log("body", req.body);
  if (req.body.item_id && req.body.key && req.body.value) {
    const key = req.body.key;
    const value = req.body.value;
    console.log("edit item by key");

    const update = { [key]: value };
    const filter = { _id: req.body.item_id };

    let doc = await Items.findOneAndUpdate(filter, update, { new: true });

    await doc.save(function (err, doc) {
      if (err) {
        sendJsonResponse(res, 404, err);
      } else {
        console.log("success");
        sendJsonResponse(res, 200, { message: "Successfully saved" });
      }
    });
  } else {
    sendJsonResponse(res, 404, { message: "Error" });
  }
};

//edit item by id
module.exports.editItemById = async function (req, res) {
  if (req.body._id) {
    const filter = { _id: req.body._id };
    const update = {
      images: req.body.images,
      name: req.body.name,
      price: req.body.price,
      quantityToAdd: req.body.quantityToAdd,
      category: req.body.category,
      description: req.body.description,
      brand: req.body.brand,
      dimension: req.body.dimension,
      weight: req.body.weight,
      color: req.body.color,
      other_details: req.body.other_details,
    };
    let doc = await Items.findOneAndUpdate(filter, update);
    await doc.save(function (err, doc) {
      if (err) {
        sendJsonResponse(res, 404, err);
      } else {
        sendJsonResponse(res, 200, { message: "Successfully saved" });
      }
    });
  } else {
    sendJsonResponse(res, 404, { message: "Error" });
  }
};

//delete a comment
module.exports.deleteOneComment = async function (req, res) {
  console.log("delete a comment", req.body);
  if (req.body) {
    let doc = await Items.findOneAndUpdate(
      { _id: req.body.item_id },
      {
        $pull: {
          comments: {
            submitted_by: req.body.comment.submitted_by,
            comment: req.body.comment.comment,
          },
        },
      },
      { safe: true }
    );

    await doc.save(function (err, doc) {
      if (err) {
        sendJsonResponse(res, 404, err);
      } else {
        sendJsonResponse(res, 200, {
          message: "Successfully deleted a comment",
        });
      }
    });
  } else {
    sendJsonResponse(res, 404, { message: "Error" });
  }
};

//delete item by id
module.exports.deleteItemById = async function (req, res) {
  console.log("delete item by id", req.params.itemId);
  if (req.params.itemId) {
    let doc = await Items.findOneAndDelete({_id:req.params.itemId});

    await doc.save(function (err, doc) {
      if (err) {
        sendJsonResponse(res, 404, err);
      } else {
        sendJsonResponse(res, 200, { message: "Successfully Deleted" });
      }
    });
  } else {
    sendJsonResponse(res, 404, { message: "Item not found" });
  }
};

//add new item
module.exports.addItem = function (req, res) {
  console.log("add new item");
  var id = ObjectId();
  var temp_other_details = {};

  if (req.body.other_details) {
    var temp = req.body.other_details;
    for (var i = 0; i < temp.length; i++) {
      temp_other_details[temp[i][0]] = temp[i][1];
    }
  }
  console.log("others", temp_other_details);
  Items.create(
    {
      _id: id,
      name: req.body.name,
      price: req.body.price,
      quantityToAdd: req.body.quantityToAdd,
      category: req.body.category,
      description: req.body.description,
      images: req.body.images,
      brand: req.body.brand,
      dimension: req.body.dimension,
      weight: req.body.weight,
      color: req.body.color,
      other_details: temp_other_details,
    },
    function (err, item) {
      if (err) {
        console.log("error found", err);
        sendJsonResponse(res, 404, { message: "Error adding new item" });
      } else {
        sendJsonResponse(res, 200, {
          message: "Successfully added the item to db",
        });
      }
    }
  );
};

//get item by id
module.exports.getItemById = async function (req, res) {
  console.log("get item by id", req.params.itemId);
  await Items.findById(req.params.itemId).exec(function (err, item) {
    if (!item) {
      sendJsonResponse(res, 404, { message: "Item id not found" });
      // return;
    } else if (err) {
      sendJsonResponse(res, 404, err);
    } else {
      sendJsonResponse(res, 200, { item: item });
    }
  });
};

//get items
module.exports.getAllItems = async function (req, res) {
  // console.log("get items ");
  await Items.find({}).exec(function (err, data) {
    if (!data.length) {
      console.log("no data found in db");
      sendJsonResponse(res, 400, {
        message: "empty database: no blogs to display",
      });
    } else if (err) {
      console.log("no db found", err);
      sendJsonResponse(res, 400, { message: "error fetching data from db" });
    } else {
      console.log("data found in db");
      sendJsonResponse(res, 200, {
        items: data,
      });
    }
  });
};
