var mongoose = require("mongoose");
var ObjectId = require("mongodb").ObjectId;
var User = mongoose.model("User");
var Items = mongoose.model("Items");
var OrderList = mongoose.model("OrderList");

var sendJsonResponse = function (res, status, content) {
  res.status(status);
  res.json(content);
};

//undo order completed
module.exports.undoOrderCompleted = async function (req, res) {
  if (req.body.order_id) {
    const filter = { _id: req.body.order_id };
    const update = { order_completed: "NO" };

    let doc = await OrderList.findOneAndUpdate(filter, update);
    await doc.save(function (err, doc) {
      if (err) {
        sendJsonResponse(res, 404, err);
      } else {
        sendJsonResponse(res, 200, { message: "Successfully saved" });
      }
    });
  } else {
    
    sendJsonResponse(res, 404, { message: "Error" });
  }
};
//order completed
module.exports.orderCompleted = async function (req, res) {
  if (req.body.order_id) {
    const filter = { _id: req.body.order_id };
    const update = { order_completed: "YES" };

    let doc = await OrderList.findOneAndUpdate(filter, update);
    await doc.save(function (err, doc) {
      if (err) {
        sendJsonResponse(res, 404, err);
      } else {
        sendJsonResponse(res, 200, { message: "Successfully saved" });
      }
    });
  } else {
    
    sendJsonResponse(res, 404, { message: "Error" });
  }
};

//get completed orders list
module.exports.getCompletedOrdersList = async function (req, res) {
  await OrderList.find({ order_completed: "YES" }).exec(function (err, list) {
    if (!list) {
      sendJsonResponse(res, 404, { message: "Items not found" });
      // return;
    } else if (err) {
      sendJsonResponse(res, 404, err);
    } else {
      sendJsonResponse(res, 200, { list: list });
    }
  });
};
//get pending orders list
module.exports.getPendingOrdersList = async function (req, res) {
  await OrderList.find({ order_completed: "NO" }).exec(function (err, list) {
    if (!list) {
      sendJsonResponse(res, 404, { message: "Items not found" });
      // return;
    } else if (err) {
      sendJsonResponse(res, 404, err);
    } else {
      sendJsonResponse(res, 200, { list: list });
    }
  });
};

//get order by id
module.exports.getOrderById = function (req, res) {
  console.log("get order by id", req.params.order_id);

  OrderList.findById(req.params.order_id).exec(function (err, order) {
    if (!order) {
      sendJsonResponse(res, 404, { message: "Order id not found" });
    } else if (err) {
      sendJsonResponse(res, 404, err);
    } else {
      sendJsonResponse(res, 200, { order: order });
    }
  });
};
// add to order list
module.exports.addOrderToList = function (req, res) {
  if (req.body) {
    OrderList.create(
      {
        username: req.body.username,
        user_id: req.body.user_id,
        shopping_cart: req.body.shopping_cart,
        order_date: req.body.order_date,
        total: req.body.total,
        order_completed: req.body.order_completed,
      },
      function (err, order) {
        if (err) {
          console.log("error found", err);
          sendJsonResponse(res, 404, {
            message: "Error adding new order to database",
          });
        } else {
          console.log("added to db");
          sendJsonResponse(res, 200, {
            message: "Successfully added order to database",
          });
        }
      }
    );
  }
};

//get order list
module.exports.getOrderList = function (req, res) {
  console.log("inside api getorderlist");
  OrderList.find({}).exec(function (err, data) {
    if (!data.length) {
      console.log("no order list data found in db");
      sendJsonResponse(res, 400, {
        message: "empty database: no order list found",
      });
    } else if (err) {
      console.log("no db found", err);
      sendJsonResponse(res, 400, {
        message: "error fetching data from db",
      });
    } else {
      console.log("data found in db");
      sendJsonResponse(res, 200, {
        orderList: data,
      });
    }
  });
};
