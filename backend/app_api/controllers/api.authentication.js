var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');
var AdminUser = mongoose.model('AdminUser');



var sendJSONresponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};

//register admin trap if admin already exists
module.exports.registerAdminTrap = function (req, res) {
    console.log('contact admin');
    sendJSONresponse(res, 200, {"message":"Contact super Admin/IT Admin to register an Admin"});
}

//register admin when using for the first time.
module.exports.registerAdmin = function(req, res){
    if(!req.body.name || !req.body.email || !req.body.password ){
        sendJSONresponse(res, 400, {
            "message": "All fields required"
        });
        return;
    }

    var user = new AdminUser();
    user.name = req.body.name;
    user.email = req.body.email;
    user.setPassword(req.body.password);

    user.save(function(err, data){
        var token;

        if(err){

            console.log('error in register',err);
            sendJSONresponse(res, 404, err);
        }else{

            token = user.generateJwt();
            sendJSONresponse(res, 200, {
                "token": token,
                "message": "Successfully registered an Admin"
            });
        }
    });

};

module.exports.register = function(req, res){
    if(!req.body.name || !req.body.email || !req.body.password ){
        sendJSONresponse(res, 400, {
            "message": "All fields required"
        });
        return;
    }

    var user = new User();

    user.name = req.body.name;
    user.email = req.body.email;
    user.setPassword(req.body.password);

    user.save(function(err, data){
        var token;

        if(err){

            console.log('error in register',err);
            sendJSONresponse(res, 404, err);
        }else{

            token = user.generateJwt();
            sendJSONresponse(res, 200, {
                "token": token,
                "message": 'Successfully, registered.'
            });
        }
    });

};

//login function, has different logic for admin and general users.
module.exports.login = function(req, res){
    console.log('obj ', req.body.email, req.body.password)
    console.log('admin ', req.body.admin_user)
    if(!req.body.email || !req.body.password){
        sendJSONresponse(res, 400, {
            "message": "All fields required"
        });
        return;
    }
    passport.authenticate('local', function(err, user, info){
        var token;
        if(err){
            sendJSONresponse(res, 404, err);
        }
        if(user){
            token = user.generateJwt();
            // console.log(user);
            var message = "Successfully Logged in"
            if(req.body.admin_user){
                message += ' as Admin';
            }
            console.log('api ',user._id);
            sendJSONresponse(res, 200, {
                "token": token,
                "username": user.name,
                "_id" : user._id,
                "message": message,
            });
        }else{
          console.log('error error')
            sendJSONresponse(res, 401, info);
        }

    })(req, res); //make sure req and res are available to Passport
};







