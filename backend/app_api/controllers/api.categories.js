var mongoose = require("mongoose");
var ObjectId = require("mongodb").ObjectId;
var Items = mongoose.model("Items");

const categories = ["1", "2", "3", "4", "5"];

var sendJsonResponse = function (res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.getCategoriesList = function (req, res) {
  sendJsonResponse(res, 200, {
    categories,
  });
};

module.exports.getItemsByCategory = function (req, res) {
  Items.find({category: req.params.category}, function (err, items) {
    if(!items){
      console.log('no');
    }else if(err){
      console.log('err');
    }else{
      console.log('items found');
      sendJsonResponse(res,200,{"items": items});
    }
  })
};
